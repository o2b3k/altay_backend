<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['prefix' => '/'], function (){
    Route::get('/', 'FrontendController@index')->name('index');
    Route::get('/search', 'FrontendController@search')->name('front.Search');
    Route::get('/kvartira/{slug}', 'FrontendController@apartmentShow')->name('front.Show');
    Route::post('/send/message', 'FrontendController@sendMessage')->name('front.SendMessage');
    Route::get('/uslugi/{slug}', 'FrontendController@blogShow')->name('front.BlogShow');
    Route::get('/about', 'FrontendController@about')->name('front.About');
    Route::get('/contact', 'FrontendController@contact')->name('front.Contact');
});

Auth::routes();

Route::get('/home', function (){
    return redirect()->route('admin.Dashboard');
})->name('home');

Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function (){
    //Dashboard Controller
    Route::get('/dashboard', 'ComplainController@dashboard')->name('admin.Dashboard');
    //Category Controller
    Route::get('/category', 'CategoryController@index')->name('category.Index');
    Route::get('/category/add', 'CategoryController@create')->name('category.Create');
    Route::post('/category/add', 'CategoryController@store')->name('category.Store');
    Route::get('/category/edit/{category}', 'CategoryController@edit')->name('category.Edit');
    Route::post('/category/edit/{category}', 'CategoryController@update')->name('category.Update');
    Route::delete('/category/destroy', 'CategoryController@destroy')->name('category.Destroy');
    //Agent Controller
    Route::get('/agent', 'AgentController@index')->name('agent.Index');
    Route::get('/agent/add', 'AgentController@create')->name('agent.Create');
    Route::post('/agent/add', 'AgentController@store')->name('agent.Store');
    Route::get('/agent/edit/{agent}', 'AgentController@edit')->name('agent.Edit');
    Route::post('agent/edit/{agent}', 'AgentController@update')->name('agent.Update');
    Route::delete('/agent/destroy', 'AgentController@destroy')->name('agent.Destroy');
    //Apartment Controller
    Route::get('/apartment', 'ApartmentController@index')->name('apartment.Index');
    Route::get('/apartment/add', 'ApartmentController@create')->name('apartment.Create');
    Route::post('/apartment/add', 'ApartmentController@store')->name('apartment.Store');
    Route::get('/apartment/edit/{apartment}', 'ApartmentController@edit')->name('apartment.Edit');
    Route::post('/apartment/edit/{apartment}', 'ApartmentController@update')->name('apartment.Update');
    Route::delete('/apartment/destroy', 'ApartmentController@destroy')->name('apartment.Destroy');
    Route::get('/apartment/image/upload/{apartment}', 'ApartmentController@uploadForm')->name('apartment.UploadForm');
    Route::post('/apartment/image/upload/{apartment}', 'ApartmentController@uploadImage')->name('apartment.UploadImage');
    Route::delete('/apartment/image/destroy/{id}', 'ApartmentController@destroyImage')->name('apartment.ImageDestroy');
    Route::post('/apartment/setActive', 'ApartmentController@setActive')->name('apartment.SetActive');
    //Region Controller
    Route::get('/region', 'RegionController@index')->name('region.Index');
    Route::get('/region/add', 'RegionController@create')->name('region.Create');
    Route::post('/region/add', 'RegionController@store')->name('region.Store');
    Route::get('/region/edit/{region}', 'RegionController@edit')->name('region.Edit');
    Route::post('/region/edit/{region}', 'RegionController@update')->name('region.Update');
    Route::delete('/region/destroy', 'RegionController@destroy')->name('region.Destroy');
    //Blog controller
    Route::get('/blog', 'BlogController@index')->name('blog.Index');
    Route::get('/blog/add', 'BlogController@create')->name('blog.Create');
    Route::post('/blog/add', 'BlogController@store')->name('blog.Store');
    Route::get('/blog/edit/{blog}', 'BlogController@edit')->name('blog.Edit');
    Route::post('/blog/edit/{blog}', 'BlogController@update')->name('blog.Update');
    Route::delete('/blog/destroy', 'BlogController@destroy')->name('blog.Destroy');
    //Type Controller
    Route::get('/type', 'TypeController@index')->name('type.Index');
    Route::get('/type/add', 'TypeController@create')->name('type.Create');
    Route::post('/type/add', 'TypeController@store')->name('type.Store');
    Route::get('/type/edit/{type}', 'TypeController@edit')->name('type.Edit');
    Route::post('/type/edit/{type}', 'TypeController@update')->name('type.Update');
    Route::delete('/type/destroy', 'TypeController@destroy')->name('type.Destroy');
    //Complain
    Route::get('/complain', 'ComplainController@index')->name('complain.Index');
    Route::get('/complain/view/{complain}', 'ComplainController@show')->name('complain.Show');
    Route::delete('/complain/destroy', 'ComplainController@destroy')->name('complain.Destroy');
    //Arenda
    Route::get('/sales', 'SalesController@index')->name('sales.Index');
    Route::delete('/sales/destroy', 'SalesController@destroy')->name('sales.Destroy');
    Route::get('/sales/add', 'SalesController@create')->name('sales.Create');
    Route::post('/sales/add', 'SalesController@store')->name('sales.Store');
    Route::get('/sales/edit/{sales}', 'SalesController@edit')->name('sales.Edit');
    Route::post('/sales/edit/{sales}', 'SalesController@update')->name('sales.Update');
    Route::get('/sales/price/asc', 'SalesController@getPriceAsc')->name('sales.PriceAsc');
    Route::get('/sales/price/desc', 'SalesController@getPriceDesc')->name('sales.PriceDesc');
    //Dom
    Route::get('/dom', 'DomController@index')->name('dom.Index');
    Route::get('/dom/create', 'DomController@create')->name('dom.Create');
    Route::post('/dom/create', 'DomController@store')->name('dom.Store');
    Route::get('/dom/edit/{dom}', 'DomController@edit')->name('dom.Edit');
    Route::post('/dom/edit/{dom}', 'DomController@update')->name('dom.Update');
    Route::delete('/dom/destroy', 'DomController@destroy')->name('dom.Destroy');
    Route::get('/dom/price/asc', 'DomController@getPriceAsc')->name('dom.PriceAsc');
    Route::get('/dom/price/desc', 'DomController@getPriceDesc')->name('dom.PriceDesc');
});