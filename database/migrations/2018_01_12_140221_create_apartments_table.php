<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApartmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('apartments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('number');
            $table->string('name',250);
            $table->integer('rooms')->nullable();
            $table->string('series',250)->nullable();
            $table->integer('floor')->nullable();
            $table->string('square')->nullable();
            $table->string('district',250)->nullable();
            $table->string('address',250)->nullable();
            $table->string('price',250)->nullable();
            $table->text('desc')->nullable();
            $table->string('image',250)->nullable();
            $table->string('alt',250)->nullable();
            $table->string('status')->nullable();
            $table->integer('category_id')->unsigned()->nullable();
            $table->integer('region_id')->unsigned()->nullable();
            $table->integer('agent_id')->unsigned()->nullable();
            $table->string('slug',250);
            $table->integer('type_id')->unsigned()->nullable();
            $table->boolean('active')->nullable();
            $table->integer('count')->nullable();
            $table->timestamps();
            $table->foreign('category_id')->references('id')->on('categories')->onDelete('set null');
            $table->foreign('region_id')->references('id')->on('regions')->onDelete('set null');
            $table->foreign('agent_id')->references('id')->on('agents')->onDelete('set null');
            $table->foreign('type_id')->references('id')->on('types')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('apartments');
    }
}
