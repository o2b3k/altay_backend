<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('doms', function (Blueprint $table) {
            $table->increments('id');
            $table->string('data')->nullable();
            $table->string('type')->nullable();
            $table->string('district',250)->nullable();
            $table->string('address',250)->nullable();
            $table->text('description')->nullable();
            $table->string('document',250)->nullable();
            $table->string('tel')->nullable();
            $table->integer('price')->nullable();
            $table->string('s_price')->nullable();
            $table->string('agent',250)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('doms');
    }
}
