<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales', function (Blueprint $table) {
            $table->increments('id');
            $table->string('data')->nullable();
            $table->integer('rooms')->nullable();
            $table->string('series')->nullable();
            $table->string('floor')->nullable();
            $table->string('area')->nullable();
            $table->string('district')->nullable();
            $table->string('address')->nullable();
            $table->text('description')->nullable();
            $table->string('document')->nullable();
            $table->string('tel')->nullable();
            $table->integer('price')->nullable();
            $table->string('s_price')->nullable();
            $table->string('agent')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales');
    }
}
