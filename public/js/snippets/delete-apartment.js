const deleteApartmentForm = $('#delete-apartment-form');
$('.btn-delete-apartment').click(function () {
    if (confirm('Вы действительно хотите удалить эту квартиру')){
        let $this = $(this);
        deleteApartmentForm.find('#apartment_id').val($this.data('id'));
        deleteApartmentForm.submit();
    }
});