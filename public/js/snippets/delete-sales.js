const deleteSalesForm = $('#delete-sale-form');
$('.btn-delete-sale').click(function () {
    if (confirm('Вы действительно хотите удалить эту квартиру')){
        var $this = $(this);
        deleteSalesForm.find('#sales_id').val($this.data('id'));
        deleteSalesForm.submit();
    }
});