const deleteSalesForm = $('#delete-dom-form');
$('.btn-delete-dom').click(function () {
    if (confirm('Вы действительно хотите удалить эту квартиру')){
        var $this = $(this);
        deleteSalesForm.find('#dom_id').val($this.data('id'));
        deleteSalesForm.submit();
    }
});