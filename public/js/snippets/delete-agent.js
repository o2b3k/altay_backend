const deleteAgentForm = $('#delete-agent-form');
$('.btn-delete-agent').click(function () {
    if (confirm('Вы действительно хотите удалить эту категорию')){
        var $this = $(this);
        deleteAgentForm.find('#agent_id').val($this.data('id'));
        deleteAgentForm.submit();
    }
});