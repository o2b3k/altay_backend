const deleteBlogForm = $('#delete-blog-form');
$('.btn-delete-blog').click(function () {
    if (confirm('Вы действительно хотите удалить эту услуги')){
        var $this = $(this);
        deleteBlogForm.find('#blog_id').val($this.data('id'));
        deleteBlogForm.submit();
    }
});