const deleteRegionForm = $('#delete-region-form');
$('.btn-delete-region').click(function () {
    if (confirm('Вы действительно хотите удалить эту категорию')){
        var $this = $(this);
        deleteRegionForm.find('#region_id').val($this.data('id'));
        deleteRegionForm.submit();
    }
});