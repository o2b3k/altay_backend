const deleteTypeForm = $('#delete-type-form');
$('.btn-delete-type').click(function () {
    if (confirm('Вы действительно хотите удалить эту тип')){
        var $this = $(this);
        deleteTypeForm.find('#type_id').val($this.data('id'));
        deleteTypeForm.submit();
    }
});