const deleteComplainForm = $('#delete-complain-form');
$('.btn-delete-complain').click(function () {
    if (confirm('Вы действительно хотите удалить эту заявку')){
        var $this = $(this);
        deleteComplainForm.find('#complain_id').val($this.data('id'));
        deleteComplainForm.submit();
    }
});