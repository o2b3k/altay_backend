$(document).ready(function () {
    "use strict"
    var idNumber;
    var image;    
    var name;
    var description;
    // Type
    var room;
    var series;
    var floor;
    var square;
    var district;
    var addr;
    var price;
    var type;

    var dataNumber;
    var dataImage;
    var dataName;
    var dataDesc;
    // var dataType;
    var dataRoom;
    var dataSeries;
    var dataFloor;
    var dataSquare;
    var dataDistrict;
    var dataAddr;
    var dataPrice;
    var dataType;

    $('.js-preloader').fadeToggle();

    $('.show').on('click', function (event) {
        
        var id = $(this).data('id');
        $.ajax({
            type: 'GET',
            url: "/api/apartment/show",
            data: {
                '_token': $('input[name=_token]').val(),
                'id': id
            },
            success: function (data) {
                idNumber = $('#idNumber');
                image = $('#image').attr('src', data['apartment']['image']);
                name = $('#apartmentName');
                description = $('#description');
                // Type
                room = $('#rooms');
                series = $('#series');
                floor = $('#floor');
                square = $('#square');
                district = $('#district');
                addr = $('#addr');
                price = $('#price');
                type = $('#type');

                dataNumber = data['apartment']['number'];
                dataName = data['apartment']['name'];
                dataDesc = data['apartment']['desc'];
                // Type
                dataRoom = data['apartment']['rooms'];
                dataSeries = data['apartment']['series'];
                dataFloor = data['apartment']['floor'];
                dataSquare= data['apartment']['square'];
                dataDistrict = data['apartment']['district'];
                dataAddr = data['apartment']['address'];
                dataPrice = data['apartment']['price'];
                dataType = data['type'];
                var x = dataDesc.substring(0, 150);

                idNumber.html(dataNumber);
                name.html(dataName);
                description.html(x + '. . .');
                // Type
                room.html(dataRoom);
                series.html(dataSeries);
                floor.html(dataFloor);
                square.html(dataSquare);
                district.html(dataDistrict);
                addr.html(dataAddr);
                price.html(dataPrice + '$');
                type.html(dataType);
                console.log(data);
               
            },
        });
    });
    var mainText = $('.main-text-item');

    
    function SubStr(elem, limit){
        if(elem.length >= limit){
            elem = elem.substring(0, limit)
        }else{
            return elem
        }
    };

    var fooText = $('.foo_text_h3').text();
    var xxx =  fooText.substring(10, 0);
});