@extends('main')
@section('content')
    <div class="panel">
        <div class="panel-body">
            <form action="{{ isset($region) ? route('region.Update',['region' => $region]) : route('region.Store') }}" method="POST">
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-sm-12 col-ld-12">
                        <div class="form-group">
                            <h4 class="example-title">Имя</h4>
                            <input type="text" name="name" class="form-control"
                                   id="inputHelpText" value="{{ old('name', isset($region) ? $region->name : '') }}">
                            <span class="text-help">Наименование регион</span>
                            @if($errors->has('name'))
                                <span class="has-error">{{ $errors->first('name') }}</span>
                            @endif
                        </div>
                    </div>
                </div>
                <a href="{{ route('region.Index') }}" class="btn btn-danger float-sm-left">Назад</a>
                <button type="submit" class="btn btn-success float-sm-right">
                    @isset($region)
                        Изменить
                    @else
                        Добавить
                    @endisset
                </button>
            </form>
        </div>
    </div>
@endsection
@push('js')
    <script src="{{ asset('js/toastr.min.js') }}"></script>
    <script src="{{ asset('js/tos.js') }}"></script>
    @if (session('status'))
        <?php $status = session('status'); ?>
        <script>
            @if (isset($status['title']))
            toastr.{{ $status['type'] }}('{{ $status['message'] }}', '{{ $status['title'] }}');
            @else (isset($status['title']) || isset())
            toastr.{{ $status['type'] }}('{{ $status['message'] }}');
            @endif
        </script>
    @endif
@endpush
