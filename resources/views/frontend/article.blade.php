<!DOCTYPE html>
<html lang="ru">

<head>

<meta charset="utf-8">

<title>Агенство недвижимости - Алтай</title>
<meta name="description" content="Здесь вы сможете найти все варианты первичного, вторичного жилья ,квартиры, дома, участки и коммерческой недвижимости (помещения под офисы, магазины, склады и т.д.). ">

<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<meta property="og:image" content="img/shareimg">
<link rel="shortcut icon" href="/img/favicon/favicon.ico" type="image/x-icon">
<!-- <link rel="apple-touch-icon" href="img/favicon/apple-touch-icon.png">
<link rel="apple-touch-icon" sizes="72x72" href="img/favicon/apple-touch-icon-72x72.png">
<link rel="apple-touch-icon" sizes="114x114" href="img/favicon/apple-touch-icon-114x114.png"> -->

<!-- Chrome, Firefox OS and Opera -->
<meta name="theme-color" content="#000">
<!-- Windows Phone -->
<meta name="msapplication-navbutton-color" content="#000">
<!-- iOS Safari -->
<meta name="apple-mobile-web-app-status-bar-style" content="#000">

    <style>
        body {
            opacity: 0;
            overflow-x: hidden;
        }

        html {
            background-color: #fff;
        }
    </style>

</head>

<body>

    <div id="app" class="wrapper_site">
        <div id="navigation" class="navbar_hader">
            <div class="header">
                <div class="container">
                    <div class="row">
                        <div class="contact d-flex justify-content-between w-100 align-items-center">
                            <div class="company-info d-flex justify-content-start align-items-center">
                                <div class="contact_item">
                                    <em class="ion-ios-email pr-2"></em>
                                    <div class="contact_span">altay.osoo@mail.ru</div>
                                </div>
                                <div class="line"></div>
                                <div class="contact_item">
                                    <em class="ion-android-call pr-2"></em>
                                    <div class="contact_span">+996 (555) 208-181, +996 (708) 182-228</div>
                                </div>
                            </div>
                            <div class="line"></div>
                            <div class="contact_span">Курс: 68.20 сом</div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="line_horizontal"></div>

            <nav class="nav single-nav">
                <div class="container">
                    <div class="row">
                        <div class="navigation d-flex justify-content-between align-items-center w-100">
                            <div class="logo d-flex align-items-center justify-content-start">
                                <img src="{{ asset('img/logo.png') }}" alt="">
                                <div class="logo_text ">
                                    <span class="nav_brand">АЛТАЙ</span>
                                    <br>
                                    <p class="nav_desc">Агенство недвижимости</p>
                                </div>
                            </div>
                            <div class="phone_btn">
                                <em class="phone_icon-bars ion-navicon"></em>
                            </div>
                            @include('frontend.menu')
                        </div>
                    </div>
                </div>
            </nav>
        </div>


        <div id="custom" class="custom-esate single-article-page">
            <div class="path d-flex align-items-center justify-content-start">
                <a href="{{ route('index') }}" class="path-item">Главная</a>
                <em class="ion-ios-arrow-right path-item"></em>
                <span class="path-item">{{ $blog->title }}</span>
            </div>
            <div class="container p_t">
                <div class="row">
                    <div class="article_heading w-100 pb-3">
                        <h2 class="h3">{{ $blog->title }}</h2>
                    </div>
                   
                    <p class="description">{!! $blog->desc !!}</p>
                </div>
            </div>
        </div>


        <div id="footer" class="footer p_t">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-md-4">
                        <div class="foo-heading mt-5">
                            <h2 class="h5">О компании</h2>
                        </div>
                        <p class="description mt-5">
                            Мы ответственны в своей работе и заботимся об удобстве наших клиентов, поэтому в целях экономии Вашего времени создан сайт
                            АН «Алтай. База постоянно обновляется. Здесь вы сможете найти все варианты первичного, вторичного
                            жилья ,квартиры, дома, участки и коммерческой недвижимости (помещения под офисы, магазины, склады
                            и т.д.).
                        </p>
                        <div class=" mt-5 logo d-flex align-items-center justify-content-start">
                            <img src="{{ asset('img/logo.png') }}" alt="">
                            <div class="logo_text ">
                                <span class="nav_brand">АЛТАЙ</span>
                                <br>
                                <p class="nav_desc">Агенство недвижимости</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-4">
                        <div class="foo-heading mt-5">
                            <h2 class="h5">Последние добавленные</h2>
                        </div>
                        <div class="mt-5">
                            @foreach($latest as $apartment)
                                <div class="foo-lates d-flex align-items-center justify-content-between">
                                    <div class="foo-img pr-2">
                                        <img src="{{ asset($apartment->image) }}" class="img-fluid" alt="">
                                    </div>
                                    <div class="foo-text ">
                                        <a href="{{ route('front.Show',['slug' => $apartment->slug]) }}">
                                            <h3 class="h6">{{ $apartment->name }}</h3>
                                        </a>
                                        <span class="price">
                                            {{ $apartment->price }}$
                                        </span>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-4">
                        <div class="foo-heading mt-5">
                            <h2 class="h5">Контакты</h2>
                        </div>
                        <div class="mt-5">
                            <div class="d-flex mb-2">
                                <em class="ion-ios-location"></em>
                                <p class="description pl-4">Кыргызстан, Бишкек ул. Токтогула/Пр.мира, 173 кв. 2</p>
                            </div>
                            <div class="d-flex  mb-2">
                                <em class="ion-ios-email"></em>
                                <p class="description pl-4">altay.osoo@mail.ru</p>
                            </div>
                            <div class="d-flex  mb-2">
                                <em class="ion-android-globe"></em>
                                <p class="description pl-4">http://altay.kg</p>
                            </div>
                            <div class="d-flex  mb-2">
                                <em class="ion-android-phone-portrait"></em>
                                <p class="description pl-4">+996 (555) 208-181, +996 (708) 182-228</p>
                            </div>
                            <div class="d-flex 	 mb-2">
                                <em class="ion-android-call"></em>
                                <p class="description pl-4">+312 31-30-86</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <hr>
        <div class="copyright pt-2 pb-2">
            <div class="container">
                <div class="row">
                    <p class="description">ALTAY © 2018 Все права защишены</p>
                </div>
            </div>
        </div>
        <!-- CSS -->
        <link rel="stylesheet" href="{{ elixir('css/app.css') }}">
        <!-- JS -->
        <script src="{{ elixir('js/scripts.min.js') }}"></script>
        <script src="{{ asset('libs/jquery/dist/slick.min.js') }}"></script>
        <script src="http://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.js"></script>
</body>

</html>