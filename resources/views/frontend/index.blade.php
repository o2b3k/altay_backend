<!DOCTYPE html>
<html lang="ru">

<head>

    <meta charset="utf-8">

    <title>Агенство недвижимости - Алтай</title>
    <meta name="description" content="Здесь вы сможете найти все варианты первичного, вторичного жилья ,квартиры, дома, участки и коммерческой недвижимости (помещения под офисы, магазины, склады и т.д.). ">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <meta property="og:image" content="img/shareimg">
    <link rel="shortcut icon" href="img/favicon/favicon.ico" type="image/x-icon">
    <!-- <link rel="apple-touch-icon" href="img/favicon/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="img/favicon/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="img/favicon/apple-touch-icon-114x114.png"> -->

    <!-- Chrome, Firefox OS and Opera -->
    <meta name="theme-color" content="#000">
    <!-- Windows Phone -->
    <meta name="msapplication-navbutton-color" content="#000">
    <!-- iOS Safari -->
    <meta name="apple-mobile-web-app-status-bar-style" content="#000">

    <style>
        body {
            opacity: 0;
            overflow-x: hidden;
        }

        html {
            background-color: #fff;
        }
    </style>

</head>

<body>
    <!-- Для Формы -->
    <div class="js-preloader">
        <div class="icon">
        <i class="fa fa-cog fa-spin"></i>
        </div>
    </div>

    <div class="remodal" data-remodal-id="modal" data-remodal-options="hashTracking: false, closeOnOutsideClick: false">

        <button data-remodal-action="close" class="remodal-close"></button>
        <h1>Оставить заявку</h1>
        <form action="{{ route('front.SendMessage') }}" method="post">
            {{ csrf_field() }}
            <div class="remodal-body">
                <div class="form-group">
                    <label for="name">Ваше имя</label>
                    <input type="text" class="form-control" name="name" id="name" placeholder="Введите имя">
                </div>
                <div class="form-group">
                    <label for="phone">Номер телефона</label>
                    <input type="text" class="form-control" name="phone" id="phone" placeholder="+996 (555) 555-555">
                </div>
                <div class="form-group">
                    <label for="message">Сообщение</label>
                    <textarea class="form-control" name="message" cols="30" rows="7"></textarea>
                </div>
            </div>
            <br>
            <button type="submit" class="btn btn-accent">Отправить</button>
        </form>
    </div>

    <!-- Для Просмотра -->

    <div class="remodal remodal-eye" data-remodal-id="eye" data-remodal-options="hashTracking: false, closeOnOutsideClick: false">

        <button data-remodal-action="close" class="remodal-close"></button>
        <div class="remodal-body">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xs-12 col-lg-7 pl-0">
                        <div class="eye-img">
                        <img id="image" src="">
                        </div>
                    </div>
                    <div class="col-xs-12 col-lg-5 pr-0">
                        <h3 class="h4" id="apartmentName"></h3>
                        <hr>
                        <p class="description" id="description">
                            <a href="#">Подробнее</a>
                        </p>
                        <div class="mt-3 pb-1 remodal-item d-flex justify-content-between align-items-center w-100">
                            <strong>Тип</strong>
                            <span id="type"></span>
                        </div>
                        <div class="mt-3 pb-1 remodal-item d-flex justify-content-between align-items-center w-100">
                            <strong>Кол-во комнат</strong>
                            <span id="rooms"></span>
                        </div>
                        <div class="mt-3 pb-1 remodal-item d-flex justify-content-between align-items-center w-100">
                            <strong>Серия</strong>
                            <span id="series"></span>
                        </div>
                        <div class="mt-3 pb-1 remodal-item d-flex justify-content-between align-items-center w-100">
                            <strong>Этаж</strong>
                            <span id="floor"></span>
                        </div>
                        <div class="mt-3 pb-1 remodal-item d-flex justify-content-between align-items-center w-100">
                            <strong>м
                                <sup>2</sup>
                            </strong>
                            <span id="square"></span>
                        </div>
                        <div class="mt-3 pb-1 remodal-item d-flex justify-content-between align-items-center w-100">
                            <strong>Район</strong>
                            <span id="district"></span>
                        </div>
                        <div class="mt-3 pb-1 remodal-item d-flex justify-content-between align-items-center w-100">
                            <strong>Адрес</strong>
                            <span id="addr"></span>
                        </div>
                        <div class="mt-3 pb-1 remodal-item d-flex justify-content-between align-items-center w-100">
                            <strong>Цена</strong>
                            <span id="price"></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <div class="w-100" style="position: relative; margin-top: 0px; top: 15px; display: block">
            <button data-remodal-action="cancel" id="close" class="btn btn-default">Закрыть</button>
        </div>
    </div>


    <div id="app" class="wrapper_site">
        <div id="navigation" class="navbar_hader">
            <div class="header">
                <div class="container">
                    <div class="row">
                        <div class="contact d-flex justify-content-between w-100 align-items-center">
                            <div class="company-info d-flex justify-content-start align-items-center">
                                <div class="contact_item">
                                    <em class="ion-ios-email pr-2"></em>
                                    <div class="contact_span">altay.osoo@mail.ru</div>
                                </div>
                                <div class="line"></div>
                                <div class="contact_item">
                                    <em class="ion-android-call pr-2"></em>
                                    <div class="contact_span">(552) 290-190, (775) 290-890, (500) 290-890</div>
                                </div>
                            </div>
                            <div class="line"></div>
                            <div class="contact_span">Курс: 68.20 сом</div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="line_horizontal"></div>

            <nav class="nav">
                <div class="container">
                    <div class="row">
                        <div class="navigation d-flex justify-content-between align-items-center w-100">
                            <div class="logo d-flex align-items-center justify-content-start">
                                <img src="{{ asset('img/logo.png') }}" alt="">
                                <div class="logo_text ">
                                    <span class="nav_brand">АЛТАЙ</span>
                                    <br>
                                    <p class="nav_desc">Агенство недвижимости</p>
                                </div>
                            </div>
                            <div class="phone_btn">
                                <em class="phone_icon-bars ion-navicon"></em>
                            </div>
                            @include('frontend.menu')
                        </div>
                    </div>
                </div>
            </nav>
        </div>

        <div id="top" class="main_content">
            <figure class="figure_content">
                <div class="img-main_content">
                    <img src="{{ asset('img/bg1.jpg') }}" class="img-fluid" alt="Агенство недвижимости" class="main_img">
                </div>
                <div class="container">
                    <div class="row">
                        <figcaption class="figure-caption">
                            <h1 class="h5">Агенство недвижимости ОсОО "АЛТАЙ"</h1>
                            <div class="main-text">
                                <p>Найди свой уголок</p>
                            </div>
                            <p class="h6">Продажа и обмен всех видов недвижимости</p>
                            <div class="pt-2">
                                <a href="#sell-plus" class="btn btn-accent mr-2">Купить</a>
                                <a data-remodal-target="modal" href="#" class="btn btn-default">Позвонить</a>
                            </div>
                        </figcaption>
                    </div>
                </div>
            </figure>
            <div id="filter" class="sarch_filter  pt-4 pb-4 d-flex align-items-center">
                <div class="container">
                    <div class="row">
                        <div class="search-icon">
                            <em class="ion-search"></em>
                        </div>
                        <form action="{{ route('front.Search') }}" method="GET" class="w-100">
                            {{ csrf_field() }}
                            <div class="search-filder d-flex justify-content-between align-items-end w-100">
                                <div class="searc-group w-25">
                                    <label for="search-id" class="search-label">ID Недвижимости</label>
                                    <input type="text" name="number" class="form-control" placeholder="Введите ID">
                                </div>
                                <div class="searc-group w-25">
                                    <label for="search-location" class="search-label">Категория</label>
                                    <select name="region" id="" class="form-control">
                                            <option value=""></option>
                                        @foreach($regions as $region)
                                            <option value="{{ $region->name }}">{{ $region->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="searc-group w-25">
                                    <label for="search-price" class="search-label">Тип</label>
                                    <select name="type" id="" class="form-control">
                                            <option value=""></option>
                                        @foreach($types as $type)
                                            <option value="{{ $type->name }}">{{ $type->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="searc-group w-25">
                                    <label for="search-status" class="search-label">Статус</label>
                                    <select name="status" class="form-control">
                                            <option value=""></option>
                                        @foreach(\App\Models\Apartment::getStatus(true) as $status => $name)
                                            <option value="{{ $status }}">{{ $name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="searc-group w-25">
                                    <input type="submit" value="Применить" class="btn btn-accent">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div id="sell-plus" class="sell-estate">
            <div class="heading text-center w-100">
                <h2 class="h6 heading_head">Выгодные предложения</h2>
                <p class="h5 heading_paraph">Успейте приобрести</p>
            </div>

            <!-- SLick -->
            <div class="slick-button w-100 text-center">
                <button type="button" class="prev">Prev</button>
                <button type="button" class="next">Next</button>
            </div>

            <div class="container">
                <div class="estate">
                    @foreach($actives as $active)
                        <div>
                        <div class="item">
                            <div class="img-item">
                                <a href="{{ route('front.Show',['slug' => $active->slug]) }}">
                                    <img src="{{ $active->image }}" alt="" class="img-fluid">
                                </a>
                               
                                @if($active->status == 'exchange')<div class="obmen"><span>Обмен</span></div>  @else   <div class="lenta"><span> Продажа </span></div>@endif
                                <div class="id">
                                    <span>ID {{ $active->number }}</span>
                                </div>
                            </div>
                            <div class="p-3 item-block">
                                <div class="text-item d-flex justify-content-between align-items-center w-100">
                                    <h3 class="h6">{!! \App\Models\Apartment::getName($active->name) !!}</h3>
                                    <span class="price">{{ $active->price }}$</span>
                                </div>
                                <div class="param-item">
                                    <div class="icon-item d-flex justify-content-between align-items-center flex-wrap">
                                        <div class="param">
                                            <em class="ion-android-pin pr-2"></em>
                                            <span>{{ $active->address }}</span>
                                        </div>
                                        <div class="param">
                                            <em class="ion-android-contacts pr-2"></em>
                                            <span>{{ $active->rooms }}</span>
                                        </div>
                                        <div class="param">
                                            <em class="ion-waterdrop pr-2"></em>
                                            <span>3</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>

        <div id="custom" class="custom-esate">
            <div class="heading text-center w-100 pb-3">
                <h2 class="h6 heading_head">Последние предложения</h2>
                <p class="h5 heading_paraph">Вся недвижимость</p>
            </div>

            <div class="container">
                <div class="row">
                    @foreach($apartments as $apartment)
                    <div class="col-lg-12 mt-5 custom_item-esate">
                        <figure class="d-flex h-100 item_custom-figure">
                            <div class="custom-img">
                                <img src="{{ $apartment->image }}" class="img-fluid" alt="{{ $apartment->alt }}">
                                    @if($apartment->status == 'exchange') <div class="obmen"><span>Обмен</span></div>  @else   <div class="lenta"><span> Продажа </span></div>@endif
                                <div class="id">
                                    <span id="number">ID {{ $apartment->number }}</span>
                                </div>
                            </div>
                            <figcaption class="h-100 w-100">
                                <div class="h-100">
                                    <div class="p-4 item-block w-100 h-100">
                                        <div class="text-item d-flex justify-content-between align-items-center w-100">
                                            <h3 class="h6" class="main-text-item">{{ $apartment->name }}</h3>
                                            <span class="price price">{{ $apartment->price }}$</span>
                                        </div>
                                        <div class="param-item">
                                            <div class="icon-item d-flex justify-content-start align-items-center flex-wrap">
                                                <div class="param pr-3">
                                                    <em class="ion-android-pin pr-2"></em>
                                                    <span>
                                                        <strong class="pr-2">Адрес:</strong>{{ $apartment->address }}</span>
                                                </div>
                                                <div class="param pr-3">
                                                    <em class="ion-android-contacts pr-2"></em>
                                                    <span>
                                                        <strong class="pr-2">Кол-во комнат:</strong>{{ $apartment->rooms }}</span>
                                                </div>
                                                <div class="param pr-3">
                                                    <em class="ion-waterdrop pr-2"></em>
                                                    <span>
                                                        <strong class="pr-2">Серия:</strong>{{ $apartment->series }}</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="pb-2"></div>
                                        <hr>
                                        <p class="description">
                                            {!! \App\Models\Apartment::getExcerpt($apartment->desc) !!}
                                        </p>
                                        <div class="readmore text-right mt-5">
                                            <a id="show" data-remodal-target="eye" href="#" class="btn btn-accent mr-2 show" data-id="{{ $apartment->id }}">Просмотр</a>
                                            <a href="{{ route('front.Show',['slug' => $apartment->slug]) }}" class="btn btn-default">Подробнее</a>
                                        </div>
                                    </div>
                                </div>
                            </figcaption>
                        </figure>
                    </div>
                    @endforeach
                </div>
                <div class="load">
                    <a href="#" id="loadMore" class="loadMore text-center btn btn-accent">Загрузить еще</a>
                </div>
            </div>
        </div>

        <div id="services" class="services p_t">
            <div class="heading text-center w-100">
                <h2 class="h6 heading_head">Услуги</h2>
                <p class="h5 heading_paraph">Мы предлагаем следующее</p>
            </div>
            <div class="container mt-5">
                <div class="service">
                    @foreach($blogs as $blog)
                        <div>
                            <div class="serve-item">
                                <div class="img-service">
                                    <img src="{{ asset($blog->image) }}" class="img-fluid" alt="">
                                </div>
                                <div class="text-service">
                                    <div class="pt-3 pb-1 d-flex justify-content-between align-items-center flex-wrap">
                                        <h3 class="h5">{!! \App\Models\Blog::getTitle($blog->title) !!}</h3>
                                        <span class="date">{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $blog->created_at)->toDateString()}}</span>
                                    </div>
                                    <hr>
                                    <div class="description service-desc">
                                        <p>{!! \App\Models\Blog::getExcerpt($blog->desc) !!}</p>
                                    </div>
                                    <div class="readmore text-right">
                                        <a href="{{ route('front.BlogShow',['slug' => $blog->slug]) }}" class="btn btn-accent">Подробнее</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>

        <div id="footer" class="footer p_t">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-md-4">
                        <div class="foo-heading mt-5">
                            <h2 class="h5">О компании</h2>
                        </div>
                        <p class="description mt-5">
                            Мы ответственны в своей работе и заботимся об удобстве наших клиентов, поэтому в целях экономии Вашего времени создан сайт
                            АН «Алтай. База постоянно обновляется. Здесь вы сможете найти все варианты первичного, вторичного
                            жилья ,квартиры, дома, участки и коммерческой недвижимости (помещения под офисы, магазины, склады
                            и т.д.).
                        </p>
                        <div class=" mt-5 logo d-flex align-items-center justify-content-start">
                            <img src="img/logo.png" alt="">
                            <div class="logo_text ">
                                <span class="nav_brand">АЛТАЙ</span>
                                <br>
                                <p class="nav_desc">Агенство недвижимости</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-4">
                        <div class="foo-heading mt-5">
                            <h2 class="h5">Последние добавленные</h2>
                        </div>
                        <div class="mt-5">
                            @foreach($latest as $apartment)
                                <div class="foo-lates d-flex align-items-center justify-content-between">
                                    <div class="foo-img pr-2">
                                        <img src="{{ asset($apartment->image) }}" class="img-fluid" alt="">
                                    </div>
                                    <div class="foo-text ">
                                        <a href="{{ route('front.Show',['slug' => $apartment->slug]) }}">
                                            <h3 class="h6 foo_text_h3">{{ $apartment->name }}</h3>
                                        </a>
                                        <span class="price">
                                            {{ $apartment->price }}$
                                        </span>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-4">
                        <div class="foo-heading mt-5">
                            <h2 class="h5">Контакты</h2>
                        </div>
                        <div class="mt-5">
                            <div class="d-flex mb-2">
                                <em class="ion-ios-location"></em>
                                <p class="description pl-4">Кыргызстан, Бишкек ул. Токтогула/Пр.мира, 173 кв. 2</p>
                            </div>
                            <div class="d-flex  mb-2">
                                <em class="ion-ios-email"></em>
                                <p class="description pl-4">altay.osoo@mail.ru</p>
                            </div>
                            <div class="d-flex  mb-2">
                                <em class="ion-android-globe"></em>
                                <p class="description pl-4">http://altay.kg</p>
                            </div>
                            <div class="d-flex  mb-2">
                                <em class="ion-android-phone-portrait"></em>
                                <p class="description pl-4">(552) 290-190, (775) 290-890, (500) 290-890</p>
                            </div>
                            <div class="d-flex 	 mb-2">
                                <em class="ion-android-call"></em>
                                <p class="description pl-4">+312 31-30-86</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <hr>
        <div class="copyright pt-2 pb-2">
            <div class="container">
                <div class="row">
                    <p class="description">ALTAY © 2018 Все права защишены</p>
                </div>
            </div>
        </div>



        <!-- CSS -->
        <link rel="stylesheet" href="{{ elixir('css/app.css') }}">
        <!-- JS -->
        <script src="https://code.jquery.com/jquery-2.2.4.js" integrity="sha256-iT6Q9iMJYuQiMWNd9lDyBUStIq/8PuOW33aOqmvFpqI=" crossorigin="anonymous"></script>
        <script src="{{ elixir('js/scripts.min.js') }}"></script>
        <script src="{{ asset('libs/jquery/dist/slick.min.js') }}"></script>
        <script src="{{ asset('js/custom.js') }}"></script>
        <script src="{{ asset('libs/jquery/dist/remodal.js') }}"></script>
        <script src="http://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.js"></script>
        <!-- 16 KB -->

        <script>
            $(document).ready(function () {

            })
        </script>
    </div>
</body>

</html>