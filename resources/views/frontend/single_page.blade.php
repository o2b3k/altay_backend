<!DOCTYPE html>
<html lang="ru">

<head>

<meta charset="utf-8">

<title>{{ $apartment->name }} - Агенство недвижимости Алтай</title>
<meta name="description" content="{!! $apartment->desc !!}">

<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<meta property="og:image" content="{{ $apartment->image }}">
<link rel="shortcut icon" href="/img/favicon/favicon.ico" type="image/x-icon">
<!-- <link rel="apple-touch-icon" href="img/favicon/apple-touch-icon.png">
<link rel="apple-touch-icon" sizes="72x72" href="img/favicon/apple-touch-icon-72x72.png">
<link rel="apple-touch-icon" sizes="114x114" href="img/favicon/apple-touch-icon-114x114.png"> -->

<!-- Chrome, Firefox OS and Opera -->
<meta name="theme-color" content="#000">
<!-- Windows Phone -->
<meta name="msapplication-navbutton-color" content="#000">
<!-- iOS Safari -->
<meta name="apple-mobile-web-app-status-bar-style" content="#000">

	<style>
		body {
			opacity: 0;
			overflow-x: hidden;
		}

		html {
			background-color: #fff;
		}
	</style>

</head>

<body>

	<div id="app" class="wrapper_site">
		<div id="navigation" class="navbar_hader">
			<div class="header">
				<div class="container">
					<div class="row">
						<div class="contact d-flex justify-content-between w-100 align-items-center">
							<div class="company-info d-flex justify-content-start align-items-center">
								<div class="contact_item">
									<em class="ion-ios-email pr-2"></em>
									<div class="contact_span">altay.osoo@mail.ru</div>
								</div>
								<div class="line"></div>
								<div class="contact_item">
									<em class="ion-android-call pr-2"></em>
									<div class="contact_span">+996 (555) 208-181, +996 (708) 182-228</div>
								</div>
							</div>
							<div class="line"></div>
							<div class="contact_span">Курс: 68.20 сом</div>
						</div>
					</div>
				</div>
			</div>

			<div class="line_horizontal"></div>

			<nav class="nav single-nav">
				<div class="container">
					<div class="row">
						<div class="navigation d-flex justify-content-between align-items-center w-100">
							<a href="{{ route('index') }}">
							<div class="logo d-flex align-items-center justify-content-start">
								<img src="{{ asset('img/logo.png') }}" alt="">
								<div class="logo_text ">
									<span class="nav_brand">АЛТАЙ</span>
									<br>
									<p class="nav_desc">Агенство недвижимости</p>
								</div>
							</div>
							</a>
							<div class="phone_btn">
								<em class="phone_icon-bars ion-navicon"></em>
							</div>
							@include('frontend.menu')
						</div>
					</div>
				</div>
			</nav>
		</div>


		<div id="custom" class="custom-esate">
			<div class="path d-flex align-items-center justify-content-start">
				<a href="/" class="path-item">Главная</a>
				<em class="ion-ios-arrow-right path-item"></em>
				<span class="path-item">{{ $apartment->name }}</span>
			</div>
			<div class="container p_t">
				<div class="row">
					<div class="col-lg-8">
						<div class="deailt-single-heading">

							<h2 class="h3 detail-heading">{{ $apartment->name }}</h2>
						</div>
						<div class="d-flex justify-content-between align-items-center mb-3">
								<div class="date  d-flex align-items-center"><em class="ion-ios-calendar-outline pr-2"></em><span>{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $apartment->created_at)->toDateString()}}</span></div>
								<div class="view d-flex align-items-center"><em class="ion-eye pr-2"></em><span>{{ $apartment->count }}</span></div>
						</div>
						<div class="container">
							<div class="row">
								<div class="col-lg-12 p-0">
									<div class="eye-img single-img">
									<!-- <div>
										<img src="/img/bg1.jpg" class="img-fluid" alt="">
									</div> -->
										<div class="fotorama" data-height="420px"  data-width="100%" data-nav="thumbs" data-thumbwidth="225" data-thumbheight="125" data-navposition="left">
											@isset($apartment->photos)
												@foreach($apartment->photos as $photo)
													<img src="{{ asset($photo->path) }}" class="img-fluid" alt="{{ $photo->alt }}">
												@endforeach
											@endisset
										</div>
									</div>
								</div>

								<div class="col-lg-12 mt-5">
									<div class="aside_desc">
										<h3 class="h5">Краткая информация:</h3>
										<hr>
										<p class="description">{!! $apartment->desc !!}</p>
									</div>
									<div class=" text-right  mt-3 ">
										<script type="text/javascript">
											(function () {
												if (window.pluso)
													if (typeof window.pluso.start == "function") return;
												if (window.ifpluso == undefined) {
													window.ifpluso = 1;
													var d = document,
														s = d.createElement('script'),
														g = 'getElementsByTagName';
													s.type = 'text/javascript';
													s.charset = 'UTF-8';
													s.async = true;
													s.src = ('https:' == window.location.protocol ? 'https' : 'http') + '://share.pluso.ru/pluso-like.js';
													var h = d[g]('body')[0];
													h.appendChild(s);
												}
											})();
										</script>
										<div class="pluso" data-background="transparent" data-options="medium,round,line,horizontal,counter,theme=04" data-services="vkontakte,odnoklassniki,facebook,twitter,google,moimir,email,print"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-4">
						<div class="detail-agent mb-3">
							<h3 class="h4 aside-param m-0">Агент</h3>
							<div class="d-flex agent align-items-center">
								@isset($apartment->agent)
									<div class="aside-avatar">
										@if($apartment->agent->image)
											<img src="{{ asset($apartment->agent->image) }}" class="mr-3 img-responsive" alt="">
											@else
											<img src="" class="mr-3 img-responsive" alt="">
										@endif
									</div>
									<div class="aside-contact">
										<strong>Агент:</strong> {{ $apartment->agent->fio }} <br>
										<strong>Телефон:</strong> {{ $apartment->agent->phone }}
									</div>
								@endisset
							</div>
						</div>
						<div class="alert alert-info">
							<p class="description"><strong>Примечание:</strong> Если у вас возникли вопросы вы можете написать нам в режиме онлайн и задать Вас интересующие вопросы</p>
						</div>
						<div class="detail-estate-param">
							<h3 class="h4 aside-param m-0">Информация о предложении</h3>
							<div class="detail-est">
							<div class="mt-3 pb-1 remodal-item d-flex justify-content-between align-items-center w-100">
								<strong>Тип</strong>
								<span>
									@isset($apartment->type)
										{{ $apartment->type->name }}
									@endisset
								</span>
							</div>
							<div class="mt-3 pb-1 remodal-item d-flex justify-content-between align-items-center w-100">
								<strong>Кол-во комнат</strong>
								<span>{{ $apartment->rooms }}</span>
							</div>
							<div class="mt-3 pb-1 remodal-item d-flex justify-content-between align-items-center w-100">
								<strong>Серия</strong>
								<span>{{ $apartment->series }}</span>
							</div>
							<div class="mt-3 pb-1 remodal-item d-flex justify-content-between align-items-center w-100">
								<strong>Этаж</strong>
								<span>{{ $apartment->floor }}</span>
							</div>
							<div class="mt-3 pb-1 remodal-item d-flex justify-content-between align-items-center w-100">
								<strong>м
									<sup>2</sup>
								</strong>
								<span>{{ $apartment->square }}</span>
							</div>
							<div class="mt-3 pb-1 remodal-item d-flex justify-content-between align-items-center w-100">
								<strong>Район</strong>
								<span>
									@isset($apartment->region)
										{{ $apartment->region->name }}
									@endisset
								</span>
							</div>
							<div class="mt-3 pb-1 remodal-item d-flex justify-content-between align-items-center w-100">
								<strong>Адрес</strong>
								<span>{{ $apartment->address }}</span>
							</div>
							<div class="mt-3 pb-1 remodal-item d-flex justify-content-between align-items-center w-100">
								<strong>Цена</strong>
								<span>{{ $apartment->price }}$</span>
							</div>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>

		<div id="services" class="services p_t">
			<div class="heading text-center w-100">
				<h2 class="h6 heading_head">Услуги</h2>
				<p class="h5 heading_paraph">Мы предлагаем следующее</p>
			</div>
			<div class="container mt-5">
				<div class="service">
					@foreach($blogs as $blog)
						<div>
							<div class="serve-item">
								<div class="img-service">
									<img src="{{ asset($blog->image) }}" class="img-fluid" alt="">
								</div>
								<div class="text-service">
									<div class="pt-3 pb-1 d-flex justify-content-between align-items-center flex-wrap">
										<h3 class="h5">{!! \App\Models\Blog::getTitle($blog->title) !!}</h3>
										<span class="date">{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $blog->created_at)->toDateString()}}</span>
									</div>
									<hr>
									<div class="description service-desc">
										<p>{!! \App\Models\Blog::getExcerpt($blog->desc) !!}</p>
									</div>
									<div class="readmore text-right">
										<a href="{{ route('front.BlogShow',['slug' => $blog->slug]) }}" class="btn btn-accent">Подробнее</a>
									</div>
								</div>
							</div>
						</div>
					@endforeach
				</div>
			</div>
		</div>

		<div id="footer" class="footer p_t">
			<div class="container">
				<div class="row">
					<div class="col-sm-12 col-md-4">
						<div class="foo-heading mt-5">
							<h2 class="h5">О компании</h2>
						</div>
						<p class="description mt-5">
							Мы ответственны в своей работе и заботимся об удобстве наших клиентов, поэтому в целях экономии Вашего времени создан сайт
							АН «Алтай. База постоянно обновляется. Здесь вы сможете найти все варианты первичного, вторичного жилья ,квартиры,
							дома, участки и коммерческой недвижимости (помещения под офисы, магазины, склады и т.д.).
						</p>
						<div class=" mt-5 logo d-flex align-items-center justify-content-start">
							<img src="{{ asset('img/logo.png') }}" alt="">
							<div class="logo_text ">
								<span class="nav_brand">АЛТАЙ</span>
								<br>
								<p class="nav_desc">Агенство недвижимости</p>
							</div>
						</div>
					</div>
					<div class="col-sm-12 col-md-4">
						<div class="foo-heading mt-5">
							<h2 class="h5">Последние добавленные</h2>
						</div>
						<div class="mt-5">
							@foreach($latest as $apartment)
								<div class="foo-lates d-flex align-items-center justify-content-between">
									<div class="foo-img pr-2">
										<img src="{{ asset($apartment->image) }}" class="img-fluid" alt="">
									</div>
								<div class="foo-text ">
										<a href="{{ route('front.Show',['slug' => $apartment->slug]) }}">
											<h3 class="h6">{{ $apartment->name }}</h3>
										</a>
										<span class="price">
											{{ $apartment->price }}$
										</span>
									</div>
								</div>
							@endforeach
						</div>
					</div>
					<div class="col-sm-12 col-md-4">
						<div class="foo-heading mt-5">
							<h2 class="h5">Контакты</h2>
						</div>
						<div class="mt-5">
							<div class="d-flex mb-2">
								<em class="ion-ios-location"></em>
								<p class="description pl-4">Кыргызстан, Бишкек ул. Токтогула/Пр.мира, 173 кв. 2</p>
							</div>
							<div class="d-flex  mb-2">
								<em class="ion-ios-email"></em>
								<p class="description pl-4">altay.osoo@mail.ru</p>
							</div>
							<div class="d-flex  mb-2">
								<em class="ion-android-globe"></em>
								<p class="description pl-4">http://altay.kg</p>
							</div>
							<div class="d-flex  mb-2">
								<em class="ion-android-phone-portrait"></em>
								<p class="description pl-4">+996 (555) 208-181, +996 (708) 182-228</p>
							</div>
							<div class="d-flex 	 mb-2">
								<em class="ion-android-call"></em>
								<p class="description pl-4">+312 31-30-86</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<hr>
		<div class="copyright pt-2 pb-2">
			<div class="container">
				<div class="row">
					<p class="description">ALTAY © 2018 Все права защишены</p>
				</div>
			</div>
		</div>
		<!-- CSS -->
		<link rel="stylesheet" href="{{ elixir('css/app.css') }}">
		<!-- JS -->
		<script src="https://code.jquery.com/jquery-2.2.4.js" integrity="sha256-iT6Q9iMJYuQiMWNd9lDyBUStIq/8PuOW33aOqmvFpqI=" crossorigin="anonymous"></script>
		<script src="{{ elixir('js/scripts.min.js') }}"></script>
		<script src="{{ asset('libs/jquery/dist/slick.min.js') }}"></script>
		<script src="http://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.js"></script>
</body>

</html>