<div class="navbar-nav" style="width: 50%">
    <ul class="menu d-flex p-0 m-0  d-flex w-100 justify-content-between" style="list-style-type: none">
        <li>
            <a href="{{ route('index') }}" class="menu-link">Главная</a>
        </li>
        <li>
            <a href="/#services" class="menu-link">Услуги</a>
        </li>
        <li>
            <a href="/uslugi/karera-v-agenestvo-nedvizhimosti-altay" class="menu-link">Вакансии</a>
        </li>
        <li>
            <a href="{{ route('front.About') }}" class="menu-link">О нас</a>
        </li>
        <li>
            <a href="{{ route('front.Contact') }}" class="menu-link">Контакты</a>
        </li>
    </ul>
</div>