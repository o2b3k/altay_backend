@extends('main')
@section('content')
    <div class="panel">
        <div class="panel-body">
            <form action="{{ isset($dom) ? route('dom.Update',['dom' => $dom]) : route('dom.Store') }}" method="POST">
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-sm-6 col-ld-6">
                        <div class="form-group">
                            <h4 class="example-title">Дата</h4>
                            <input type="text" name="data" class="form-control"
                                   id="inputHelpText" value="{{ old('data', isset($dom) ? $dom->data : '') }}">
                            @if($errors->has('data'))
                                <span class="has-error">{{ $errors->first('data') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-sm-6 col-ld-6">
                        <div class="form-group">
                            <h4 class="example-title">Тип</h4>
                            <select name="type" id="parent" class="form-control">
                                <option value="дом">Дом</option>
                                <option value="участок">Участок</option>
                                <option value="п-дом">П-дом</option>
                            </select>
                            @if($errors->has('rooms'))
                                <span class="has-error">{{ $errors->first('rooms') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-sm-6 col-ld-6">
                        <div class="form-group">
                            <h4 class="example-title">Район</h4>
                            <input type="text" name="district" class="form-control"
                                   id="inputHelpText" value="{{ old('district', isset($dom) ? $dom->district : '') }}">
                            @if($errors->has('district'))
                                <span class="has-error">{{ $errors->first('district') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-sm-6 col-ld-6">
                        <div class="form-group">
                            <h4 class="example-title">Адрес</h4>
                            <input type="text" name="address" class="form-control"
                                   id="inputHelpText" value="{{ old('address', isset($dom) ? $dom->address : '') }}">
                            @if($errors->has('address'))
                                <span class="has-error">{{ $errors->first('address') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-sm-6 col-ld-6">
                        <div class="form-group">
                            <h4 class="example-title">Описание</h4>
                            <input type="text" name="description" class="form-control"
                                   id="inputHelpText" value="{{ old('description', isset($dom) ? $dom->description : '') }}">
                            @if($errors->has('description'))
                                <span class="has-error">{{ $errors->first('description') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-sm-6 col-ld-6">
                        <div class="form-group">
                            <h4 class="example-title">Документы</h4>
                            <input type="text" name="document" class="form-control"
                                   id="inputHelpText" value="{{ old('document', isset($dom) ? $dom->document : '') }}">
                            @if($errors->has('document'))
                                <span class="has-error">{{ $errors->first('document') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-sm-6 col-ld-6">
                        <div class="form-group">
                            <h4 class="example-title">Телефон</h4>
                            <input type="text" name="tel" class="form-control"
                                   id="inputHelpText" value="{{ old('tel', isset($dom) ? $dom->tel : '') }}">
                            @if($errors->has('tel'))
                                <span class="has-error">{{ $errors->first('tel') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-sm-6 col-ld-6">
                        <div class="form-group">
                            <h4 class="example-title">Цена</h4>
                            <input type="text" name="price" class="form-control"
                                   id="inputHelpText" value="{{ old('price', isset($dom) ? $dom->price : '') }}">
                            @if($errors->has('price'))
                                <span class="has-error">{{ $errors->first('price') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-sm-6 col-ld-6">
                        <div class="form-group">
                            <h4 class="example-title">Руки</h4>
                            <input type="text" name="s_price" class="form-control"
                                   id="inputHelpText" value="{{ old('s_price', isset($dom) ? $dom->s_price : '') }}">
                            @if($errors->has('s_price'))
                                <span class="has-error">{{ $errors->first('s_price') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-sm-6 col-ld-6">
                        <div class="form-group">
                            <h4 class="example-title">Агент</h4>
                            <input type="text" name="agent" class="form-control"
                                   id="inputHelpText" value="{{ old('agent', isset($dom) ? $dom->agent : '') }}">
                            @if($errors->has('agent'))
                                <span class="has-error">{{ $errors->first('agent') }}</span>
                            @endif
                        </div>
                    </div>
                </div>
                <a href="{{ route('dom.Index') }}" class="btn btn-danger float-sm-left">Назад</a>
                <button type="submit" class="btn btn-success float-sm-right">
                    @isset($sale)
                        Изменить
                    @else
                        Добавить
                    @endisset
                </button>
            </form>
        </div>
    </div>
@endsection
@push('js')
    <script src="{{ asset('js/toastr.min.js') }}"></script>
    <script src="{{ asset('js/tos.js') }}"></script>
    @if (session('status'))
        <?php $status = session('status'); ?>
        <script>
            @if (isset($status['title']))
            toastr.{{ $status['type'] }}('{{ $status['message'] }}', '{{ $status['title'] }}');
            @else (isset($status['title']) || isset())
            toastr.{{ $status['type'] }}('{{ $status['message'] }}');
            @endif
        </script>
    @endif
@endpush
