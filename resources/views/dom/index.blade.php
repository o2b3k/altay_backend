<style>
    #exampleFixedHeaderHome {
        font-size: 13px;
    }

    #exampleFixedHeaderHome th {
        font-weight: 600;
        color: #555;
    }

</style>
<script>
    function myFunction() {
        var input, filter, table, tr, td, i;
        input = document.getElementById("myInputHome");
        filter = input.value.toUpperCase();
        table = document.getElementById("exampleFixedHeaderHome");
        tr = table.getElementsByTagName("tr");
        for (i = 0; i < tr.length; i++) {
            td = tr[i];
            if (td) {
                if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
            }
        }
    }

</script>
@extends('main') @section('page_header')
    <h1 class="page-title">Дом</h1>
@stop @section('content')
    <!-- Panel Table Example1 Report -->
    <div class="panel" id="exampleReport">
        <header class="panel-heading">

            <h3 class="panel-title">
                <input type="text" id="myInputHome" class="float-sm-left" placeholder="Найти..." onkeyup="myFunction()">
                    <a href="{{ route('dom.PriceAsc') }}" class="btn btn-sm btn-info float-sm-right">Цена уб.</a>
                    <a href="{{ route('dom.PriceDesc') }}" class="btn btn-sm btn-info float-sm-right">Цена воз.</a>
                    <a href="{{ route('dom.Create') }}" class="btn btn-sm btn-info float-sm-right">Добавить</a>
                <br>
            </h3>
        </header>
        <div class="panel-body">
            <div class="table-responsive" id="heightTable">
                <table class="table-bordered table table-hover dataTable table-striped" id="exampleFixedHeaderHome">
                    <thead class="thead-light">
                    <tr>
                        <th>ID</th>
                        <th>Дата</th>
                        <th>Тип</th>
                        <th>Район</th>
                        <th>Адрес</th>
                        <th>Описание</th>
                        <th>Документы</th>
                        <th>Телефон</th>
                        <th>Цена</th>
                        <th>Руки</th>
                        <th>Агент</th>
                        <th></th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($doms as $dom)
                        <tr>
                            <td>{{ $dom->id }}</td>
                            <td>{{ $dom->data }}</td>
                            <td>{{ $dom->type }}</td>
                            <td>{{ $dom->district }}</td>
                            <td>{{ $dom->address }}</td>
                            <td>{{ $dom->description }}</td>
                            <td>{{ $dom->document }}</td>
                            <td>{{ $dom->tel }}</td>
                            <td>{{ $dom->price }}</td>
                            <td>{{ $dom->s_price }}</td>
                            <td>{{ $dom->agent }}</td>
                            <td>
                                <a href="{{ route('dom.Edit',['dom' => $dom]) }}">Изменить</a>
                            </td>
                            <td>
                                <button class="btn-delete-dom" data-id="{{ $dom->id }}">Удалить</button>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>

        </div>
    </div>
    <!-- End Panel Table Example1 Report-->
    <form id="delete-dom-form" class="d-none" action="{{ route('dom.Destroy') }}" method="POST">
        {{ csrf_field() }}
        <input type="hidden" name="_method" value="DELETE">
        <input type="hidden" id="dom_id" name="dom_id">
    </form>
@stop
@push('js')
    <script type="text/javascript" src="{{ asset('js/snippets/delete-dom.js') }}"></script>
@endpush