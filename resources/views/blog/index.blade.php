@extends('main')
@section('page_header')
    <h1 class="page-title">Услуги</h1>
@stop
@section('content')
    <!-- Panel Table Example1 Report -->
    <div class="panel" id="exampleReport">
        <header class="panel-heading">
            <h3 class="panel-title">
                <a href="{{ route('blog.Create') }}" class="btn btn-sm btn-info float-sm-right">Добавить</a>
            </h3>
        </header>
        <div class="panel-body">
            <div class="table-responsive">
                <table class="table table-hover dataTable table-striped" id="exampleFixedHeader">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Наименование</th>
                        <th>Ссылка</th>
                        <th>Действия</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($blogs as $blog)
                        <tr>
                            <td>{{ $blog->id }}</td>
                            <td>{{ $blog->title }}</td>
                            <td>{{ $blog->slug }}</td>
                            <td>
                                <a href="{{ route('blog.Edit',['blog' => $blog]) }}" class="btn btn-sm btn-success">Изменить</a>
                                <button type="button" data-id="{{ $blog->id }}" class="btn btn-sm btn-danger btn-delete-blog">
                                    Удалить
                                </button>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- End Panel Table Example1 Report-->
    <form id="delete-blog-form" class="d-none" action="{{ route('blog.Destroy') }}" method="POST">
        {{ csrf_field() }}
        <input type="hidden" name="_method" value="DELETE">
        <input type="hidden" id="blog_id" name="blog_id">
    </form>
@stop
@push('js')
    <script type="text/javascript" src="{{ asset('js/snippets/delete-blog.js') }}"></script>
@endpush
@push('js')
    <script src="{{ asset('js/toastr.min.js') }}"></script>
    <script src="{{ asset('js/tos.js') }}"></script>
    @if (session('status'))
        <?php $status = session('status'); ?>
        <script>
            @if (isset($status['title']))
            toastr.{{ $status['type'] }}('{{ $status['message'] }}', '{{ $status['title'] }}');
            @else (isset($status['title']) || isset())
            toastr.{{ $status['type'] }}('{{ $status['message'] }}');
            @endif
        </script>
    @endif
@endpush