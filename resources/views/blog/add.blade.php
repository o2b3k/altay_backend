@extends('main')
@push('css')
    <!-- File upload -->
    <link rel="stylesheet" href="{{ asset('global/vendor/blueimp-file-upload/jquery.fileupload.min.css?v4.0.1') }}">
    <link rel="stylesheet" href="{{ asset('global/vendor/dropify/dropify.min.css?v4.0.1') }}">
@endpush
@section('content')
    <div class="panel">
        <div class="panel-body">
            <form action="{{ isset($blog) ? route('blog.Update',['blog' => $blog]) : route('blog.Store') }}" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-sm-12 col-ld-12">
                        <div class="form-group">
                            <h4 class="example-title">Заголовок</h4>
                            <input type="text" name="title" class="form-control"
                                   id="inputHelpText" value="{{ old('title', isset($blog) ? $blog->title : '') }}">
                            <span class="text-help">Заголовок услуги</span>
                            @if($errors->has('title'))
                                <span class="has-error">{{ $errors->first('title') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group{{ $errors->has('desc') ? 'has-error' : '' }}">
                            <h4 class="example-title">Описание</h4>
                            <textarea name="desc" class="form-control my-editor">
                                @isset($blog)
                                    {{ $blog->desc }}
                                @endisset
                            </textarea>
                            @if($errors->has('desc'))
                                <span class="has-error">{{ $errors->first('desc') }}</span>
                            @endif
                        </div>
                    </div>
                    @if(isset($blog))
                        @isset($blog->image)
                            <div class="col-sm-4 col-md-4">
                                <!-- Example Default -->
                                <div class="img-responsive">
                                    <h4 class="example-title">Изображение</h4>
                                    <div class="example">
                                        <img class="card-img-top w-full" src="{{ asset($blog->image) }}" alt="">
                                        <h4 class="example-title">Изменить</h4>
                                        <input type="file" name="image">
                                    </div>
                                </div>
                                <!-- End Example Default -->
                            </div>
                        @else
                            <div class="col-xl-4 col-md-6">
                                <!-- Example Default -->
                                <div class="example-wrap">
                                    <h4 class="example-title">Изображение</h4>
                                    <div class="example">
                                        <input type="file" name="image" id="input-file-now" data-plugin="dropify" data-default-file=""/>
                                    </div>
                                </div>
                                <!-- End Example Default -->
                            </div>
                        @endisset
                    @else
                        <div class="col-xl-4 col-md-6">
                            <!-- Example Default -->
                            <div class="example-wrap">
                                <h4 class="example-title">Изображение</h4>
                                <div class="example">
                                    <input type="file" name="image" id="input-file-now" data-plugin="dropify" data-default-file=""/>
                                </div>
                            </div>
                            <!-- End Example Default -->
                        </div>
                    @endif
                </div>
                <a href="{{ route('blog.Index') }}" class="btn btn-danger float-sm-left">Назад</a>
                <button type="submit" class="btn btn-success float-sm-right">
                    @isset($blog)
                        Изменить
                        @else
                        Добавить
                    @endisset
                </button>
            </form>
        </div>
    </div>
@endsection
@push('js')
    <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
    <script>
        var editor_config = {
            path_absolute : "/",
            language_url : '/js/ru.js',
            selector: "textarea.my-editor",
            height : "250",
            plugins: [
                "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                "searchreplace wordcount visualblocks visualchars code fullscreen",
                "insertdatetime media nonbreaking save table contextmenu directionality",
                "emoticons template paste textcolor colorpicker textpattern"
            ],
            toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
            relative_urls: false,
            file_browser_callback : function(field_name, url, type, win) {
                var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
                var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

                var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
                if (type == 'image') {
                    cmsURL = cmsURL + "&type=Images";
                } else {
                    cmsURL = cmsURL + "&type=Files";
                }

                tinyMCE.activeEditor.windowManager.open({
                    file : cmsURL,
                    title : 'Filemanager',
                    width : x * 0.8,
                    height : y * 0.8,
                    resizable : "yes",
                    close_previous : "no"
                });
            },
        };
        tinymce.init(editor_config);
    </script>
@endpush
@push('js')
    <script src="{{ asset('js/toastr.min.js') }}"></script>
    <script src="{{ asset('js/tos.js') }}"></script>
    @if (session('status'))
        <?php $status = session('status'); ?>
        <script>
            @if (isset($status['title']))
            toastr.{{ $status['type'] }}('{{ $status['message'] }}', '{{ $status['title'] }}');
            @else (isset($status['title']) || isset())
            toastr.{{ $status['type'] }}('{{ $status['message'] }}');
            @endif
        </script>
    @endif
@endpush
@push('js')
    <!-- Plugins For This Page -->
    <script src="{{ asset('global/vendor/jquery-ui/jquery-ui.min.js?v4.0.1') }}"></script>
    <script src="{{ asset('global/vendor/blueimp-tmpl/tmpl.min.js?v4.0.1') }}"></script>
    <script src="{{ asset('global/vendor/blueimp-canvas-to-blob/canvas-to-blob.min.js?v4.0.1') }}"></script>
    <script src="{{ asset('global/vendor/blueimp-load-image/load-image.all.min.js?v4.0.1') }}"></script>
    <script src="{{ asset('global/vendor/blueimp-file-upload/jquery.fileupload.js?v4.0.1') }}"></script>
    <script src="{{ asset('global/vendor/blueimp-file-upload/jquery.fileupload-process.js?v4.0.1') }}"></script>
    <script src="{{ asset('global/vendor/blueimp-file-upload/jquery.fileupload-image.js?v4.0.1') }}"></script>
    <script src="{{ asset('global/vendor/blueimp-file-upload/jquery.fileupload-audio.js?v4.0.1') }}"></script>
    <script src="{{ asset('global/vendor/blueimp-file-upload/jquery.fileupload-video.js?v4.0.1') }}"></script>
    <script src="{{ asset('global/vendor/blueimp-file-upload/jquery.fileupload-validate.js?v4.0.1') }}"></script>
    <script src="{{ asset('global/vendor/blueimp-file-upload/jquery.fileupload-ui.js?v4.0.1') }}"></script>
    <script src="{{ asset('global/vendor/dropify/dropify.min.js?v4.0.1') }}"></script>
@endpush
