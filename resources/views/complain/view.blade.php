@extends('main')
@section('content')
    <div class="panel">
        <div class="panel-body">
            <h3>Имя</h3>
            <h4>{{ $complain->name }}</h4>
            <h3>Телефон</h3>
            <h4>{{ $complain->phone }}</h4>
            <h3>Сообщение</h3>
            <p>{{ $complain->message }}</p>
        </div>
    </div>
@endsection