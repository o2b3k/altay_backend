<li class="site-menu-item">
    <a href="{{ route('admin.Dashboard') }}">
        <i class="site-menu-icon md-view-dashboard" aria-hidden="true"></i>
        <span class="site-menu-title">Статистика</span>
    </a>
</li>

<li class="site-menu-item">
    <a href="{{ route('apartment.Index') }}">
        <i class="site-menu-icon md-city-alt" aria-hidden="true"></i>
        <span class="site-menu-title">Добавить предложение</span>
    </a>
</li>

<li class="site-menu-item has-sub">
    <a href="javascript:void(0)">
        <i class="site-menu-icon md-palette" aria-hidden="true"></i>
        <span class="site-menu-title">Кастомизация</span>
        <span class="site-menu-arrow"></span>
    </a>
    <ul class="site-menu-sub">
        <li class="site-menu-item has-sub">
            <a href="{{ route('category.Index') }}">
                <span class="site-menu-title">Категория</span>
            </a>
        </li>
        <li class="site-menu-item has-sub">
            <a href="{{ route('type.Index') }}">

                <span class="site-menu-title">Тип</span>
            </a>
        </li>
        <li class="site-menu-item has-sub">
            <a href="{{ route('region.Index') }}">

                <span class="site-menu-title">Область</span>
            </a>
        </li>
        <li class="site-menu-item has-sub">
            <a href="{{ route('agent.Index') }}">

                <span class="site-menu-title">Агенты</span>
            </a>
        </li>
    </ul>
</li>

<li class="site-menu-item">
    <a href="{{ route('blog.Index') }}">
        <i class="site-menu-icon md-collection-text" aria-hidden="true"></i>
        <span class="site-menu-title">Услуги</span>
    </a>
</li>

<li class="site-menu-item">
    <a href="{{ route('complain.Index') }}">
        <i class="site-menu-icon md-comment-more" aria-hidden="true"></i>
        <span class="site-menu-title">Сообщение</span>
    </a>
</li>
<li class="site-menu-item has-sub">
    <a href="javascript:void(0)">
        <i class="site-menu-icon md-city-alt" aria-hidden="true"></i>
        <span class="site-menu-title">База данных</span>
        <span class="site-menu-arrow"></span>
    </a>
    <ul class="site-menu-sub">
        <li class="site-menu-item" style="background: #333">
            <a href="{{ route('sales.Index') }}">
                <i class="site-menu-icon md-city-alt" aria-hidden="true"></i>
                <span class="site-menu-title">Квартиры</span>
            </a>
        </li>
        <li class="site-menu-item" style="background: #333">
            <a href="{{ route('dom.Index') }}">
                <i class="site-menu-icon md-city-alt" aria-hidden="true"></i>
                <span class="site-menu-title">Дом</span>
            </a>
        </li>
    </ul>
</li>

<li class="site-menu-item">
    <a href="">
        <i class="site-menu-icon md-info" aria-hidden="true"></i>
        <span class="site-menu-title">Служба поддержки</span>
    </a>
</li>