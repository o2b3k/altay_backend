@extends('main')
@push('css')
    <!-- File upload -->
    <link rel="stylesheet" href="{{ asset('global/vendor/blueimp-file-upload/jquery.fileupload.min.css?v4.0.1') }}">
    <link rel="stylesheet" href="{{ asset('global/vendor/dropify/dropify.min.css?v4.0.1') }}">
@endpush
@section('content')
    <div class="panel">
        <div class="panel-body">
            <form action="{{ route('agent.Store') }}" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-xl-4 col-md-6">
                        <!-- Example Default -->
                        <div class="example-wrap">
                            <h4 class="example-title">Изображение</h4>
                            <div class="example">
                                <input type="file" name="image" id="input-file-now" data-plugin="dropify" data-default-file=""/>
                            </div>
                        </div>
                        <!-- End Example Default -->
                    </div>
                    <div class="col-sm-12 col-ld-12">
                        <div class="form-group">
                            <h4 class="example-title">Ф.И.О</h4>
                            <input type="text" name="fio" class="form-control"
                                   id="inputHelpText" value="{{ old('fio') }}">
                            @if($errors->has('fio'))
                                <span class="has-error">{{ $errors->first('fio') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-sm-12 col-ld-12">
                        <div class="form-group">
                            <h4 class="example-title">Телефон</h4>
                            <input type="text" name="phone" class="form-control"
                                   id="inputHelpText" value="{{ old('phone') }}" placeholder="+996 XXX XX XX XX">
                            @if($errors->has('phone'))
                                <span class="has-error">{{ $errors->first('phone') }}</span>
                            @endif
                        </div>
                    </div>
                </div>
                <a href="{{ route('agent.Index') }}" class="btn btn-danger float-sm-left">Назад</a>
                <button type="submit" class="btn btn-success float-sm-right">Добавить</button>
            </form>
        </div>
    </div>
@endsection
@push('js')
    <script src="{{ asset('js/toastr.min.js') }}"></script>
    <script src="{{ asset('js/tos.js') }}"></script>
    @if (session('status'))
        <?php $status = session('status'); ?>
        <script>
            @if (isset($status['title']))
            toastr.{{ $status['type'] }}('{{ $status['message'] }}', '{{ $status['title'] }}');
            @else (isset($status['title']) || isset())
            toastr.{{ $status['type'] }}('{{ $status['message'] }}');
            @endif
        </script>
    @endif
@endpush
@push('js')
    <!-- Plugins For This Page -->
    <script src="{{ asset('global/vendor/jquery-ui/jquery-ui.min.js?v4.0.1') }}"></script>
    <script src="{{ asset('global/vendor/blueimp-tmpl/tmpl.min.js?v4.0.1') }}"></script>
    <script src="{{ asset('global/vendor/blueimp-canvas-to-blob/canvas-to-blob.min.js?v4.0.1') }}"></script>
    <script src="{{ asset('global/vendor/blueimp-load-image/load-image.all.min.js?v4.0.1') }}"></script>
    <script src="{{ asset('global/vendor/blueimp-file-upload/jquery.fileupload.js?v4.0.1') }}"></script>
    <script src="{{ asset('global/vendor/blueimp-file-upload/jquery.fileupload-process.js?v4.0.1') }}"></script>
    <script src="{{ asset('global/vendor/blueimp-file-upload/jquery.fileupload-image.js?v4.0.1') }}"></script>
    <script src="{{ asset('global/vendor/blueimp-file-upload/jquery.fileupload-audio.js?v4.0.1') }}"></script>
    <script src="{{ asset('global/vendor/blueimp-file-upload/jquery.fileupload-video.js?v4.0.1') }}"></script>
    <script src="{{ asset('global/vendor/blueimp-file-upload/jquery.fileupload-validate.js?v4.0.1') }}"></script>
    <script src="{{ asset('global/vendor/blueimp-file-upload/jquery.fileupload-ui.js?v4.0.1') }}"></script>
    <script src="{{ asset('global/vendor/dropify/dropify.min.js?v4.0.1') }}"></script>
@endpush