<!-- Footer -->
<footer class="site-footer">
    <div class="site-footer-legal">
        © 2018
        <a href="">
            Altay
        </a>
    </div>
    <div class="site-footer-right">
        Developer <a href="https://www.facebook.com/o2b3k" target="_blank">Bakhriddin Bakhramov</a>
    </div>
</footer>