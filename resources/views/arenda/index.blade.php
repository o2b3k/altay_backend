<style>
    #exampleFixedHeader {
        font-size: 12px;
    }

    #exampleFixedHeader th {
        font-weight: 600;
        color: #555;
    }
    .table td{
        vertical-align: middle!important
    }
</style>
<script>
    function myFunction() {
        var input, filter, table, tr, td, i;
        input = document.getElementById("myInput");
        filter = input.value.toUpperCase();
        table = document.getElementById("exampleFixedHeader");
        tr = table.getElementsByTagName("tr");
        for (i = 0; i < tr.length; i++) {
            td = tr[i];
            if (td) {
                if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
            }
        }
    }

</script>
@extends('main') @section('page_header')
<h1 class="page-title">База данных</h1>
@stop @section('content')
<!-- Panel Table Example1 Report -->
<div class="panel" id="exampleReport">
    <header class="panel-heading">

        <h3 class="panel-title">
            <input type="text" id="myInput" class="float-sm-left" placeholder="Найти..." onkeyup="myFunction()">
            <a href="{{ route('sales.PriceAsc') }}" class="btn btn-sm btn-info float-sm-right">Цена уб.</a>
            <a href="{{ route('sales.PriceDesc') }}" class="btn btn-sm btn-info float-sm-right">Цена воз.</a>
            <a href="{{ route('sales.Create') }}" class="btn btn-sm btn-info float-sm-right">Добавить</a>
            <br>
        </h3>
    </header>
    <div class="panel-body">
        <div class="table-responsive" id="heightTable">
            <table class="table-bordered table table-hover dataTable table-striped" id="exampleFixedHeader">
                <thead class="thead-light">
                    <tr>
                        <th>ID</th>
                        <th>Дата</th>
                        <th>Комната</th>
                        <th>Серия</th>
                        <th>Этаж</th>
                        <th>Площадь</th>
                        <th>Район</th>
                        <th>Адрес</th>
                        <th>Описание</th>
                        <th>Документы</th>
                        <th>Телефон</th>
                        <th>Цена</th>
                        <th>Руки</th>
                        <th>Агент</th>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($sales as $sale)
                        <tr>
                            <td>{{ $sale->id }}</td>
                            <td>{{ $sale->data }}</td>
                            <td>{{ $sale->rooms }}</td>
                            <td>{{ $sale->series }}</td>
                            <td>{{ $sale->floor }}</td>
                            <td>{{ $sale->area }}м
                                <sup>2</sup>
                            </td>
                            <td>{{ $sale->district }}</td>
                            <td>{{ $sale->address }}</td>
                            <td>{{ $sale->description }}</td>
                            <td>{{ $sale->document }}</td>
                            <td>{{ $sale->tel }}</td>
                            <td>{{ $sale->price }}</td>
                            <td>{{ $sale->s_price }}</td>
                            <td>{{ $sale->agent }}</td>
                            <td><a href="{{ route('sales.Edit',['sales' => $sale]) }}"><button class="btn btn-primary">Изменить</button></a></td>
                            <td>
                                <button class="btn-delete-sale" data-id="{{ $sale->id }}">Удалить</button>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

    </div>
</div>
<!-- End Panel Table Example1 Report-->
<form id="delete-sale-form" class="d-none" action="{{ route('sales.Destroy') }}" method="POST">
    {{ csrf_field() }}
    <input type="hidden" name="_method" value="DELETE">
    <input type="hidden" id="sales_id" name="sales_id">
</form>
@stop
@push('js')
    <script type="text/javascript" src="{{ asset('js/snippets/delete-sales.js') }}"></script>
@endpush