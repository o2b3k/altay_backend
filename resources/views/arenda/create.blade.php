@extends('main')
@section('content')
    <div class="panel">
        <div class="panel-body">
            <form action="{{ isset($sale) ? route('sales.Update',['sale' => $sale]) : route('sales.Store') }}" method="POST">
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-sm-6 col-ld-6">
                        <div class="form-group">
                            <h4 class="example-title">Дата</h4>
                            <input type="text" name="data" class="form-control"
                                   id="inputHelpText" value="{{ old('data', isset($sale) ? $sale->data : '') }}">
                            @if($errors->has('data'))
                                <span class="has-error">{{ $errors->first('data') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-sm-6 col-ld-6">
                        <div class="form-group">
                            <h4 class="example-title">Комната</h4>
                            <input type="text" name="rooms" class="form-control"
                                   id="inputHelpText" value="{{ old('rooms', isset($sale) ? $sale->rooms : '') }}">
                            @if($errors->has('rooms'))
                                <span class="has-error">{{ $errors->first('rooms') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-sm-6 col-ld-6">
                        <div class="form-group">
                            <h4 class="example-title">Серия</h4>
                            <input type="text" name="series" class="form-control"
                                   id="inputHelpText" value="{{ old('series', isset($sale) ? $sale->series : '') }}">
                            @if($errors->has('series'))
                                <span class="has-error">{{ $errors->first('series') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-sm-6 col-ld-6">
                        <div class="form-group">
                            <h4 class="example-title">Этаж</h4>
                            <input type="text" name="floor" class="form-control"
                                   id="inputHelpText" value="{{ old('floor', isset($sale) ? $sale->floor : '') }}">
                            @if($errors->has('floor'))
                                <span class="has-error">{{ $errors->first('floor') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-sm-6 col-ld-6">
                        <div class="form-group">
                            <h4 class="example-title">Площадь</h4>
                            <input type="text" name="area" class="form-control"
                                   id="inputHelpText" value="{{ old('area', isset($sale) ? $sale->area : '') }}">
                            @if($errors->has('area'))
                                <span class="has-error">{{ $errors->first('area') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-sm-6 col-ld-6">
                        <div class="form-group">
                            <h4 class="example-title">Район</h4>
                            <input type="text" name="district" class="form-control"
                                   id="inputHelpText" value="{{ old('district', isset($sale) ? $sale->district : '') }}">
                            @if($errors->has('district'))
                                <span class="has-error">{{ $errors->first('district') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-sm-6 col-ld-6">
                        <div class="form-group">
                            <h4 class="example-title">Адрес</h4>
                            <input type="text" name="address" class="form-control"
                                   id="inputHelpText" value="{{ old('address', isset($sale) ? $sale->address : '') }}">
                            @if($errors->has('address'))
                                <span class="has-error">{{ $errors->first('address') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-sm-6 col-ld-6">
                        <div class="form-group">
                            <h4 class="example-title">Описание</h4>
                            <input type="text" name="description" class="form-control"
                                   id="inputHelpText" value="{{ old('description', isset($sale) ? $sale->description : '') }}">
                            @if($errors->has('description'))
                                <span class="has-error">{{ $errors->first('description') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-sm-6 col-ld-6">
                        <div class="form-group">
                            <h4 class="example-title">Документы</h4>
                            <input type="text" name="document" class="form-control"
                                   id="inputHelpText" value="{{ old('document', isset($sale) ? $sale->document : '') }}">
                            @if($errors->has('document'))
                                <span class="has-error">{{ $errors->first('document') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-sm-6 col-ld-6">
                        <div class="form-group">
                            <h4 class="example-title">Телефон</h4>
                            <input type="text" name="tel" class="form-control"
                                   id="inputHelpText" value="{{ old('tel', isset($sale) ? $sale->tel : '') }}">
                            @if($errors->has('tel'))
                                <span class="has-error">{{ $errors->first('tel') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-sm-6 col-ld-6">
                        <div class="form-group">
                            <h4 class="example-title">Цена</h4>
                            <input type="text" name="price" class="form-control"
                                   id="inputHelpText" value="{{ old('price', isset($sale) ? $sale->price : '') }}">
                            @if($errors->has('price'))
                                <span class="has-error">{{ $errors->first('price') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-sm-6 col-ld-6">
                        <div class="form-group">
                            <h4 class="example-title">Руки</h4>
                            <input type="text" name="s_price" class="form-control"
                                   id="inputHelpText" value="{{ old('s_price', isset($sale) ? $sale->s_price : '') }}">
                            @if($errors->has('s_price'))
                                <span class="has-error">{{ $errors->first('s_price') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-sm-6 col-ld-6">
                        <div class="form-group">
                            <h4 class="example-title">Агент</h4>
                            <input type="text" name="agent" class="form-control"
                                   id="inputHelpText" value="{{ old('agent', isset($sale) ? $sale->agent : '') }}">
                            @if($errors->has('agent'))
                                <span class="has-error">{{ $errors->first('agent') }}</span>
                            @endif
                        </div>
                    </div>
                </div>
                <a href="{{ route('sales.Index') }}" class="btn btn-danger float-sm-left">Назад</a>
                <button type="submit" class="btn btn-success float-sm-right">
                    @isset($sale)
                        Изменить
                    @else
                        Добавить
                    @endisset
                </button>
            </form>
        </div>
    </div>
@endsection
@push('js')
    <script src="{{ asset('js/toastr.min.js') }}"></script>
    <script src="{{ asset('js/tos.js') }}"></script>
    @if (session('status'))
        <?php $status = session('status'); ?>
        <script>
            @if (isset($status['title']))
            toastr.{{ $status['type'] }}('{{ $status['message'] }}', '{{ $status['title'] }}');
            @else (isset($status['title']) || isset())
            toastr.{{ $status['type'] }}('{{ $status['message'] }}');
            @endif
        </script>
    @endif
@endpush
