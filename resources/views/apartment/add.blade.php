@extends('main')
@section('content')
    <div class="panel">
        <div class="panel-body">
            <form action="{{ route('apartment.Store') }}" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-sm-3 col-ld-3">
                        <div class="form-group">
                            <h4 class="example-title">ID</h4>
                            <input type="text" name="number" class="form-control"
                                value="{{ old('number') }}">
                            @if($errors->has('number'))
                                <span class="has-error">{{ $errors->first('number') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-sm-9 col-ld-9">
                        <div class="form-group">
                            <h4 class="example-title">Имя</h4>
                            <input type="text" name="name" class="form-control"
                                   id="inputHelpText" value="{{ old('name') }}">
                            <span class="text-help">Наименование квартиры</span>
                            @if($errors->has('name'))
                                <span class="has-error">{{ $errors->first('name') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-sm-4 col-ld-4">
                        <div class="form-group">
                            <h4 class="example-title">Комната</h4>
                            <input type="text" name="rooms" class="form-control"
                                   id="inputHelpText" value="{{ old('rooms') }}">
                            <span class="text-help">Количество комнаты</span>
                            @if($errors->has('rooms'))
                                <span class="has-error">{{ $errors->first('rooms') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-sm-4 col-ld-4">
                        <div class="form-group">
                            <h4 class="example-title">Серия</h4>
                            <input type="text" name="series" class="form-control"
                                   id="inputHelpText" value="{{ old('series') }}">
                            <span class="text-help">Серия квартиры</span>
                            @if($errors->has('series'))
                                <span class="has-error">{{ $errors->first('series') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-sm-4 col-ld-4">
                        <div class="form-group">
                            <h4 class="example-title">Этаж</h4>
                            <input type="text" name="floor" class="form-control"
                                   id="inputHelpText" value="{{ old('floor') }}">
                            <span class="text-help">Этаж квартиры</span>
                            @if($errors->has('floor'))
                                <span class="has-error">{{ $errors->first('floor') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-sm-6 col-ld-6">
                        <div class="form-group">
                            <h4 class="example-title">Площадь</h4>
                            <input type="text" name="square" class="form-control"
                                   id="inputHelpText" value="{{ old('square') }}">
                            <span class="text-help">Общая площадь</span>
                            @if($errors->has('square'))
                                <span class="has-error">{{ $errors->first('square') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-sm-6 col-ld-6">
                        <div class="form-group">
                            <h4 class="example-title">Цена</h4>
                            <input type="text" name="price" class="form-control"
                                   id="inputHelpText" value="{{ old('price') }}">
                            <span class="text-help">Общая площадь</span>
                            @if($errors->has('price'))
                                <span class="has-error">{{ $errors->first('price') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-sm-4 col-ld-4">
                        <div class="form-group">
                            <h4 class="example-title">Изображение</h4>
                            <input type="file" name="image" class="form-control"
                                   id="inputHelpText" value="{{ old('image') }}">
                            <span class="text-help">Главное изображение</span>
                            @if($errors->has('image'))
                                <span class="has-error">{{ $errors->first('image') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-sm-8 col-ld-8">
                        <div class="form-group">
                            <h4 class="example-title">Описание</h4>
                            <input type="text" name="alt" class="form-control"
                                   id="inputHelpText" value="{{ old('alt') }}">
                            <span class="text-help">Описание изображение</span>
                            @if($errors->has('alt'))
                                <span class="has-error">{{ $errors->first('alt') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-sm-5 col-ld-5">
                        <div class="form-group">
                            <h4 class="example-title">Область</h4>
                            <select name="region" class="form-control">
                                @foreach($regions as $region)
                                    <option value="{{ $region->id }}">{{ $region->name }}</option>
                                @endforeach
                            </select>
                            @if($errors->has('region'))
                                <span class="has-error">{{ $errors->first('region') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-sm-7 col-ld-7">
                        <div class="form-group">
                            <h4 class="example-title">Район</h4>
                            <input type="text" name="district" class="form-control"
                                   id="inputHelpText" value="{{ old('district') }}">
                            @if($errors->has('district'))
                                <span class="has-error">{{ $errors->first('district') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-sm-8 col-ld-8">
                        <div class="form-group">
                            <h4 class="example-title">Адрес</h4>
                            <input type="text" name="address" class="form-control"
                                   id="inputHelpText" value="{{ old('address') }}">
                            @if($errors->has('address'))
                                <span class="has-error">{{ $errors->first('address') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-sm-4 col-ld-4">
                        <div class="form-group">
                            <h4 class="example-title">Категория</h4>
                            <select name="category" class="form-control">
                                @foreach($categories as $category)
                                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                                @endforeach
                            </select>
                            @if($errors->has('category'))
                                <span class="has-error">{{ $errors->first('category') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-sm-6 col-ld-6">
                        <div class="form-group">
                            <h4 class="example-title">Агент</h4>
                            <select name="agent" class="form-control">
                                @foreach($agents as $agent)
                                    <option value="{{ $agent->id }}">{{ $agent->fio }}</option>
                                @endforeach
                            </select>
                            @if($errors->has('address'))
                                <span class="has-error">{{ $errors->first('address') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-sm-3 col-ld-3">
                        <div class="form-group">
                            <h4 class="example-title">Тип</h4>
                            <select name="type" class="form-control">
                                @foreach($types as $type)
                                    <option value="{{ $type->id }}">{{ $type->name }}</option>
                                @endforeach
                            </select>
                            @if($errors->has('type'))
                                <span class="has-error">{{ $errors->first('type') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-sm-3 col-ld-3">
                        <div class="form-group">
                            <h4 class="example-title">Статус</h4>
                            <select name="status" class="form-control">
                                @foreach(\App\Models\Apartment::getStatus(true) as $status => $name)
                                    <option value="{{ $status }}">{{ $name }}</option>
                                @endforeach
                            </select>
                            @if($errors->has('region'))
                                <span class="has-error">{{ $errors->first('region') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group{{ $errors->has('desc') ? 'has-error' : '' }}">
                            <h4 class="example-title">Описание</h4>
                            <textarea name="desc" class="form-control my-editor"></textarea>
                            @if($errors->has('desc'))
                                <span class="has-error">{{ $errors->first('desc') }}</span>
                            @endif
                        </div>
                    </div>
                </div>
                <a href="{{ route('category.Index') }}" class="btn btn-danger float-sm-left">Назад</a>
                <button type="submit" class="btn btn-success float-sm-right">Добавить</button>
            </form>
        </div>
    </div>
@endsection
@push('js')
    <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
    <script>
        var editor_config = {
            path_absolute : "/",
            language_url : '/js/ru.js',
            selector: "textarea.my-editor",
            height : "250",
            plugins: [
                "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                "searchreplace wordcount visualblocks visualchars code fullscreen",
                "insertdatetime media nonbreaking save table contextmenu directionality",
                "emoticons template paste textcolor colorpicker textpattern"
            ],
            toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
            relative_urls: false,
            file_browser_callback : function(field_name, url, type, win) {
                var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
                var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

                var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
                if (type == 'image') {
                    cmsURL = cmsURL + "&type=Images";
                } else {
                    cmsURL = cmsURL + "&type=Files";
                }
                
                tinyMCE.activeEditor.windowManager.open({
                    file : cmsURL,
                    title : 'Filemanager',
                    width : x * 0.8,
                    height : y * 0.8,
                    resizable : "yes",
                    close_previous : "no"
                });
                
            },
            
        };
        tinymce.init(editor_config);
    </script>
@endpush
@push('js')
    <script src="{{ asset('js/toastr.min.js') }}"></script>
    <script src="{{ asset('js/tos.js') }}"></script>
    @if (session('status'))
        <?php $status = session('status'); ?>
        <script>
            @if (isset($status['title']))
            toastr.{{ $status['type'] }}('{{ $status['message'] }}', '{{ $status['title'] }}');
            @else (isset($status['title']) || isset())
            toastr.{{ $status['type'] }}('{{ $status['message'] }}');
            @endif
        </script>
    @endif
@endpush
