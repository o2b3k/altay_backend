@extends('main')
@section('page_header')
    <h1 class="page-title">Квартиры</h1>
@stop
@section('content')
    <!-- Panel Table Example1 Report -->
    <div class="panel" id="exampleReport">
        <header class="panel-heading">
            <h3 class="panel-title">
                <a href="{{ route('apartment.Create') }}" class="btn btn-sm btn-info float-sm-right">Добавить</a>
            </h3>
        </header>
        <div class="panel-body">
            <div class="table-responsive">
                <table class="table table-hover dataTable table-striped" id="exampleFixedHeader">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Наименование</th>
                        <th>Адрес</th>
                        <th>В-пр.</th>
                        <th>Действия</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($apartments as $apartment)
                        <tr>
                            <td>{{ $apartment->id }}</td>
                            <td>{{ $apartment->name }}</td>
                            <td>{{ $apartment->address }}</td>
                            <td class="w-50">
                                <div class="checkbox-custom checkbox-primary">
                                    <input type="checkbox" class="published" data-id="{{ $apartment->id }}"
                                           @if($apartment->active) checked @endif/>
                                    <label for=""></label>
                                </div>
                            </td>
                            <td>
                                <a href="{{ route('apartment.Edit',['apartment' => $apartment]) }}" class="btn btn-icon btn-success">Изменить</a>
                                <a href="{{ route('apartment.UploadForm',['apartment' => $apartment]) }}" class="btn btn-icon btn-info">
                                    <i class="icon md-collection-folder-image"></i>
                                </a>
                                <button type="button" data-id="{{ $apartment->id }}" class="btn btn-icon btn-danger btn-delete-apartment">
                                    Удалить
                                </button>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            {{ $apartments->links('vendor.paginate_bootstrap_4') }}
        </div>
    </div>
    <!-- End Panel Table Example1 Report-->
    <form id="delete-apartment-form" class="d-none" action="{{ route('apartment.Destroy') }}" method="POST">
        {{ csrf_field() }}
        <input type="hidden" name="_method" value="DELETE">
        <input type="hidden" id="apartment_id" name="apartment_id">
    </form>
@stop
@push('js')
    <script type="text/javascript" src="{{ asset('js/snippets/delete-apartment.js') }}"></script>
@endpush
@push('js')
    <script src="{{ asset('js/toastr.min.js') }}"></script>
    <script src="{{ asset('js/tos.js') }}"></script>
    @if (session('status'))
        <?php $status = session('status'); ?>
        <script>
            @if (isset($status['title']))
            toastr.{{ $status['type'] }}('{{ $status['message'] }}', '{{ $status['title'] }}');
            @else (isset($status['title']) || isset())
            toastr.{{ $status['type'] }}('{{ $status['message'] }}');
            @endif
        </script>
    @endif
@endpush
@push('js')
    <script>
        $(document).ready(function () {
            $('.published').on('click', function(event){
                var id = $(this).data('id');
                $.ajax({
                    type: 'POST',
                    url: "{{ route('apartment.SetActive') }}",
                    data: {
                        '_token': $('input[name=_token]').val(),
                        'id': id
                    },
                    success: function(data) {
                        // empty
                    },
                });
            });
        })
    </script>

@endpush