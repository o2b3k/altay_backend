<?php

namespace App\Http\Controllers;

use App\Dom;
use Illuminate\Http\Request;

class DomController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $doms = Dom::orderBy('created_at','desc')->get();
        return view('dom.index', ['doms' => $doms]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dom.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $dom = new Dom();
        $dom->data = $request->input('data');
        $dom->type = $request->input('type');
        $dom->district = $request->input('district');
        $dom->address = $request->input('address');
        $dom->description = $request->input('description');
        $dom->document = $request->input('document');
        $dom->tel = $request->input('tel');
        $dom->price = $request->input('price');
        $dom->s_price = $request->input('s_price');
        $dom->agent = $request->input('agent');
        $dom->save();

        return redirect()->route('dom.Index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Dom  $dom
     * @return \Illuminate\Http\Response
     */
    public function show(Dom $dom)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Dom  $dom
     * @return \Illuminate\Http\Response
     */
    public function edit(Dom $dom)
    {
        return view('dom.create',['dom' => $dom]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Dom  $dom
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Dom $dom)
    {
        $dom->data = $request->input('data');
        $dom->type = $request->input('type');
        $dom->district = $request->input('district');
        $dom->address = $request->input('address');
        $dom->description = $request->input('description');
        $dom->document = $request->input('document');
        $dom->tel = $request->input('tel');
        $dom->price = $request->input('price');
        $dom->s_price = $request->input('s_price');
        $dom->agent = $request->input('agent');
        $dom->save();

        return redirect()->route('dom.Index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $request->validate([
            'dom_id'  => 'required|exists:doms,id',
        ]);
        $dom = Dom::find($request->input('dom_id'));
        $dom->delete();
        return redirect()->route('dom.Index');
    }

    public function getPriceAsc()
    {
        $dom = Dom::orderBy('price','asc')->get();
        return view('dom.index',['doms' => $dom]);
    }

    public function getPriceDesc()
    {
        $dom = Dom::orderBy('price','desc')->get();
        return view('dom.index',['doms' => $dom]);
    }

    public function rules()
    {
        return [

        ];
    }

    public function messages()
    {
        return [

        ];
    }
}
