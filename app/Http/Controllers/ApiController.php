<?php

namespace App\Http\Controllers;

use App\Models\Apartment;
use Illuminate\Http\Request;

class ApiController extends Controller
{
    public function showApartment(Request $request)
    {
        $apartment = Apartment::findOrFail($request->input('id'));
        if (!$apartment){
            return response()->json([
                'error' => 'Id is failed'
            ]);
        }
        
        return response()->json([
            'apartment' => $apartment,
            'type' => $apartment->type->name,
        ]);
    }
}
