<?php

namespace App\Http\Controllers;

use App\Models\Apartment;
use App\Models\Blog;
use App\Models\Category;
use App\Models\Complain;
use App\Models\Region;
use App\Models\Type;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Validator;
class FrontendController extends Controller
{
    public function index()
    {
        $apartments = Apartment::orderBy('created_at','desc')->get();
        $blogs = Blog::orderBy('created_at','desc')->get();
        $regions = Category::orderBy('name','asc')->get();
        $active = Apartment::where('active',true)->get();
        $types = Type::all();
        $latest = Apartment::orderBy('created_at','desc')->limit(3)->get();
        $data = [
            'apartments' => $apartments,
            'blogs' => $blogs,
            'regions' => $regions,
            'actives' => $active,
            'types' => $types,
            'latest' => $latest,
        ];
        return view('frontend.index', $data);
    }

    public function search(Request $request)
    {
        $latests = Apartment::orderBy('created_at','desc')->limit(3)->get();
        $blogs = Blog::orderBy('created_at','desc')->limit(5)->get();
        $Alltypes = Type::all();
        $Allregions = Category::orderBy('name','asc')->get();

        $queryType = '';
        $queryRegion = '';
        $queryNumber = '';
        $queryStatus = '';
        //type
        $type = $request->input('type');
        $types = Type::where('name',$type)->first();
        if ($types != null){
            $queryType = $types;
        }else{
            $queryType = null;
        }
        //region
        $region = $request->input('region');
        $regions = Category::where('name',$region)->first();
        if ($regions != null){
            $queryRegion = $regions;
        }else{
            $queryRegion = null;
        }
        //number
        $number = $request->input('number');
        if ($number != null){
            $queryNumber = $number;
        }else{
            $queryNumber = null;
        }
        //status
        $status = $request->input('status');
        if ($status != null){
            $queryStatus = $status;
        }else{
            $queryStatus = null;
        }

        if ($queryNumber != null && ($queryRegion == null && $queryType == null && $queryStatus == null)){
            $apartment = Apartment::where('number',$queryNumber)->get();
            $count = Apartment::where('number',$queryNumber)->count();
            $data = [
                'apartments' => $apartment,
                'blogs' => $blogs,
                'latest' => $latests,
                'regions' => $Allregions,
                'types' => $Alltypes,
                'count' => $count,
            ];
            return view('frontend.filter_page', $data);
        }elseif ($queryRegion != null && ($queryNumber == null && $queryType == null && $queryStatus == null)){
            $apartment = Apartment::where('category_id',$queryRegion->id)->get();
            $count = Apartment::where('category_id',$queryRegion->id)->count();
            $data = [
                'apartments' => $apartment,
                'blogs' => $blogs,
                'latest' => $latests,
                'regions' => $Allregions,
                'types' => $Alltypes,
                'count' => $count,
            ];
            return view('frontend.filter_page', $data);
        }elseif ($queryType != null && ($queryNumber == null && $queryRegion == null && $queryStatus == null)){
            $apartment = Apartment::where('type_id',$queryType->id)->get();
            $count = Apartment::where('type_id',$queryType->id)->count();
            $data = [
                'apartments' => $apartment,
                'blogs' => $blogs,
                'latest' => $latests,
                'regions' => $Allregions,
                'types' => $Alltypes,
                'count' => $count,
            ];
            return view('frontend.filter_page', $data);
        }elseif ($queryStatus != null && ($queryNumber == null && $queryRegion == null && $queryType == null)){
            $apartment = Apartment::where('status',$queryStatus)->get();
            $count = Apartment::where('status',$queryStatus)->count();
            $data = [
                'apartments' => $apartment,
                'blogs' => $blogs,
                'latest' => $latests,
                'regions' => $Allregions,
                'types' => $Alltypes,
                'count' => $count
            ];
            return view('frontend.filter_page', $data);
        }elseif (($queryRegion != null && $queryType != null) && ($queryNumber == null && $queryStatus == null)){
            $apartment = Apartment::where('category_id',$queryRegion->id)->where('type_id',$queryType->id)->get();
            $count = Apartment::where('category_id',$queryRegion->id)->where('type_id',$queryType->id)->count();
            $data = [
                'apartments' => $apartment,
                'blogs' => $blogs,
                'latest' => $latests,
                'regions' => $Allregions,
                'types' => $Alltypes,
                'count' => $count,
            ];
            return view('frontend.filter_page', $data);
        }elseif (($queryRegion != null && $queryStatus != null) && ($queryNumber == null && $queryType == null)){
            $apartment = Apartment::where('category_id',$queryRegion->id)->where('status',$status)->get();
            $count = Apartment::where('category_id',$queryRegion->id)->where('status',$status)->count();
            $data = [
                'apartments' => $apartment,
                'blogs' => $blogs,
                'latest' => $latests,
                'regions' => $Allregions,
                'types' => $Alltypes,
                'count' => $count,
            ];
            return view('frontend.filter_page', $data);
        }elseif (($queryType != null && $queryStatus != null) && ($queryNumber == null && $queryRegion == null)){
            $apartment = Apartment::where('type_id',$queryType->id)->where('status',$queryStatus)->get();
            $count = Apartment::where('type_id',$queryType->id)->where('status',$queryStatus)->count();
            $data = [
                'apartments' => $apartment,
                'blogs' => $blogs,
                'latest' => $latests,
                'regions' => $Allregions,
                'types' => $Alltypes,
                'count' => $count,
            ];
            return view('frontend.filter_page', $data);
        }elseif (($queryRegion != null && $queryType != null && $queryStatus != null) && $queryNumber == null){
            $apartment = Apartment::where('category_id',$queryRegion->id)->where('type_id',$queryType->id)
                                    ->where('status',$queryStatus)->get();
            $count = Apartment::where('category_id',$queryRegion->id)->where('type_id',$queryType->id)
                ->where('status',$queryStatus)->count();
            $data = [
                'apartments' => $apartment,
                'blogs' => $blogs,
                'latest' => $latests,
                'regions' => $Allregions,
                'types' => $Alltypes,
                'count' => $count
            ];
            return view('frontend.filter_page', $data);
        }else{
            $apartment = Apartment::all();
            $count = Apartment::all()->count();
            $data = [
                'apartments' => $apartment,
                'blogs' => $blogs,
                'latest' => $latests,
                'regions' => $Allregions,
                'types' => $Alltypes,
                'count' => $count,
            ];
            return view('frontend.filter_page', $data);
        }
    }

    public function apartmentShow($slug)
    {
        $apartment = Apartment::where('slug',$slug)->first();
        $apartment->count = $apartment->count + 1;
        $apartment->save();

        $latest = Apartment::orderBy('created_at','desc')->limit(3)->get();
        $blogs = Blog::orderBy('created_at','desc')->limit(5)->get();
        $data = [
            'apartment' => $apartment,
            'latest' => $latest,
            'blogs' => $blogs,
        ];
        return view('frontend.single_page', $data);
    }

    public function blogShow($slug)
    {
        $blog = Blog::where('slug',$slug)->first();
        $latest = Apartment::orderBy('created_at','desc')->limit(3)->get();
        $data = [
            'blog' => $blog,
            'latest' => $latest,
        ];
        return view('frontend.article',$data);
    }

    public function sendMessage(Request $request)
    {
        $validator = Validator::make($request->all(),$this->rules(),$this->messages());
        if ($validator->fails()){
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $complain = new Complain();
        $complain->name = $request->input('name');
        $complain->phone = $request->input('phone');
        $complain->message = $request->input('message');
        if ($complain->save()){
            return redirect()->route('index');
        }
    }

    public function about(){
        $latest = Apartment::orderBy('created_at','desc')->limit(3)->get();
        $data = [
            'latest' => $latest,
        ];
        return view('frontend.about',$data);
    }

    public function contact(){
        $latest = Apartment::orderBy('created_at','desc')->limit(3)->get();
        $data = [
            'latest' => $latest,
        ];
        return view('frontend.about',$data);
    }

    public function vacancy(){
        $latest = Apartment::orderBy('created_at','desc')->limit(3)->get();
        $data = [
            'latest' => $latest,
        ];
        return view('frontend.about',$data);
    }

    public function rules()
    {
        return [
            'name' => 'required',
            'phone' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Имя обязательно',
            'phone.required' => 'Телефон обязательно',
        ];
    }
}
