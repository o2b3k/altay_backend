<?php

namespace App\Http\Controllers;

use App\Models\Blog;
use Illuminate\Http\Request;
use Validator;

class BlogController extends Controller
{
    var $typeSuccess = 'success';
    var $typeError = 'error';
    var $titleSuccess = 'Успешно';
    var $titleError = 'Ошибка';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $blogs = Blog::all();
        return view('blog.index', ['blogs' => $blogs]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('blog.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),$this->rules(),$this->messages());
        if ($validator->fails()){
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $blog = new Blog();
        $blog->title = $request->input('title');
        $blog->desc = $request->input('desc');
        if ($request->file('image') != null){
            $path = $this->pathImage($request);
            $blog->image = $path;
        }
        $blog->save();
        return redirect()->route('blog.Index');

    }

    /**
     * Display the specified resource.
     *
     * @param  Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function show(Blog $blog)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function edit(Blog $blog)
    {
        return view('blog.add',['blog' => $blog]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Blog $blog)
    {
        $validator = Validator::make($request->all(),$this->rules(),$this->messages());
        if ($validator->fails()){
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $blog->title = $request->input('title');
        $blog->desc = $request->input('desc');
        if ($request->file('image') != null){
            $path = $this->pathImage($request);
            $blog->image = $path;
        }
        $blog->save();
        return redirect()->route('blog.Index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $request->validate([
            'blog_id' => 'required|exists:blogs,id',
        ]);
        $blog = Blog::find($request->input('blog_id'));
        $blog->delete();
        return redirect()->route('blog.Index');
    }

    public function rules()
    {
        return [
            'title' => 'required',
            'desc' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'title.required' => 'Заголовок обязательно',
            'desc.required' => 'Текст обязательно',
        ];
    }

    public function pathImage(Request $request)
    {
        $image = $request->file('image');
        $size = getimagesize($image->path());
        if (!$size || count($size) < 2) {
            $this->toast($request,$this->typeError,$this->titleError,'Не удалось прочитать размер изображения');
            return redirect()->back();
        }
        $fileName = md5(time());
        $path = $image->move('uploads/images', $fileName.'.'.$image->getClientOriginalExtension());
        return $path;
    }

    public function toast(Request $request,$type,$title,$message)
    {
        $request->session()->flash('status',[
            'type' => $type,
            'title' => $title,
            'message' => $message,
        ]);
    }
}
