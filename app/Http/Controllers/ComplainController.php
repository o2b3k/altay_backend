<?php

namespace App\Http\Controllers;

use App\Models\Agent;
use App\Models\Apartment;
use App\Models\Blog;
use App\Models\Complain;
use Illuminate\Http\Request;

class ComplainController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $complain = Complain::all();
        return view('complain.index',['complains' => $complain]);
    }

    /**
     * Display the specified resource.
     *
     * @param  Complain  $complain
     * @return \Illuminate\Http\Response
     */
    public function show(Complain $complain)
    {
        return view('complain.view',['complain' => $complain]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $request->validate([
            'complain_id' => 'required|exists:complains,id',
        ]);
        $complain = Complain::find($request->input('complain_id'));
        $complain->delete();
        return redirect()->route('complain.Index');
    }

    public function dashboard()
    {
        $apartment = Apartment::all()->count();
        $agent = Agent::all()->count();
        $blog = Blog::all()->count();
        $complain = Complain::all()->count();
        $data = [
            'apartment' => $apartment,
            'agent' => $agent,
            'blog' => $blog,
            'complain' => $complain
        ];
        return view('dashboard',$data);
    }
}
