<?php

namespace App\Http\Controllers;

use App\Sales;
use Illuminate\Http\Request;

class SalesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sales = Sales::orderBy('created_at','desc')->get();
        return view('arenda.index',['sales' => $sales]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('arenda.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $sales = new Sales();
        $sales->data = $request->input('data');
        $sales->rooms = $request->input('rooms');
        $sales->series = $request->input('series');
        $sales->floor = $request->input('floor');
        $sales->area = $request->input('area');
        $sales->district = $request->input('district');
        $sales->address = $request->input('address');
        $sales->description = $request->input('description');
        $sales->document = $request->input('document');
        $sales->tel = $request->input('tel');
        $sales->price = $request->input('price');
        $sales->s_price = $request->input('s_price');
        $sales->agent = $request->input('agent');
        $sales->save();

        return redirect()->route('sales.Index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Sales  $sales
     * @return \Illuminate\Http\Response
     */
    public function show(Sales $sales)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Sales  $sales
     * @return \Illuminate\Http\Response
     */
    public function edit(Sales $sales)
    {
        return view('arenda.create',['sale' => $sales]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Sales  $sales
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Sales $sales)
    {
        $sales->data = $request->input('data');
        $sales->rooms = $request->input('rooms');
        $sales->series = $request->input('series');
        $sales->floor = $request->input('floor');
        $sales->area = $request->input('area');
        $sales->district = $request->input('district');
        $sales->address = $request->input('address');
        $sales->description = $request->input('description');
        $sales->document = $request->input('document');
        $sales->tel = $request->input('tel');
        $sales->price = $request->input('price');
        $sales->s_price = $request->input('s_price');
        $sales->agent = $request->input('agent');
        $sales->save();

        return redirect()->route('sales.Index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $request->validate([
            'sales_id' => 'required|exists:sales,id',
        ]);
        $sale = Sales::find($request->input('sales_id'));
        $sale->delete();
        return redirect()->route('sales.Index');
    }

    public function getPriceAsc()
    {
        $sales = Sales::orderBy('price','asc')->get();
        return view('arenda.index',['sales' => $sales]);
    }

    public function getPriceDesc()
    {
        $sales = Sales::orderBy('price','desc')->get();
        return view('arenda.index',['sales' => $sales]);
    }

    public function rules()
    {
        return [

        ];
    }

    public function messages()
    {
        return [

        ];
    }
}
