<?php

namespace App\Http\Controllers;

use App\Models\Agent;
use App\Models\Apartment;
use App\Models\Category;
use App\Models\Photo;
use App\Models\Region;
use App\Models\Type;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\Input;
class ApartmentController extends Controller
{
    var $typeSuccess = 'success';
    var $typeError = 'error';
    var $titleSuccess = 'Успешно';
    var $titleError = 'Ошибка';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $apartments = Apartment::orderBy('created_at','desc')->paginate(10);
        return view('apartment.index',['apartments' => $apartments]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        $regions = Region::all();
        $agents = Agent::all();
        $types = Type::all();
        $data = [
            'categories' => $categories,
            'regions' => $regions,
            'agents' => $agents,
            'types' => $types,
        ];
        return view('apartment.add',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),$this->rules(),$this->messages());
        if ($validator->fails()){
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $apartment = new Apartment();
        $category = Category::find($request->input('category'));
        $region = Region::find($request->input('region'));
        $agent = Agent::find($request->input('agent'));
        $type = Type::find($request->input('type'));
        $apartment->number = $request->input('number');
        $apartment->name = $request->input('name');
        $apartment->rooms = $request->input('rooms');
        $apartment->series = $request->input('series');
        $apartment->floor = $request->input('floor');
        $apartment->square = $request->input('square');
        $apartment->district = $request->input('district');
        $apartment->address = $request->input('address');
        $apartment->price = $request->input('price');
        $apartment->status = $request->input('status');
        $apartment->desc = $request->input('desc');
        if ($request->file('image') != null){
            $path = $this->pathImage($request);
            $apartment->image = $path;
        }
        $apartment->alt = $request->input('alt');
        $apartment->region()->associate($region);
        $apartment->category()->associate($category);
        $apartment->agent()->associate($agent);
        $apartment->type()->associate($type);
        $apartment->save();
        $this->toast($request,$this->typeSuccess,$this->titleSuccess,'Квартира успешно сохранено');
        return redirect()->route('apartment.Index');
    }

    /**
     * Display the specified resource.
     *
     * @param  Apartment  $apartment
     * @return \Illuminate\Http\Response
     */
    public function show(Apartment $apartment)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Apartment  $apartment
     * @return \Illuminate\Http\Response
     */
    public function edit(Apartment $apartment)
    {
        $categories = Category::all();
        $region = Region::all();
        $agents = Agent::all();
        $types = Type::all();
        $data = [
            'categories' => $categories,
            'apartment' => $apartment,
            'regions' => $region,
            'agents' => $agents,
            'types' => $types,
        ];
        return view('apartment.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Apartment  $apartment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Apartment $apartment)
    {
        $validator = Validator::make($request->all(),$this->rules(),$this->messages());
        if ($validator->fails()){
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $category = Category::find($request->input('category'));
        $region = Region::find($request->input('region'));
        $agent = Agent::find($request->input('agent'));
        $type = Type::find($request->input('type'));
        $apartment->number = $request->input('number');
        $apartment->name = $request->input('name');
        $apartment->rooms = $request->input('rooms');
        $apartment->series = $request->input('series');
        $apartment->floor = $request->input('floor');
        $apartment->square = $request->input('square');
        $apartment->district = $request->input('district');
        $apartment->address = $request->input('address');
        $apartment->price = $request->input('price');
        $apartment->status = $request->input('status');
        $apartment->desc = $request->input('desc');
        if ($request->file('image') != null){
            $path = $this->pathImage($request);
            $apartment->image = $path;
        }
        $apartment->alt = $request->input('alt');
        $apartment->region()->associate($region);
        $apartment->category()->associate($category);
        $apartment->agent()->associate($agent);
        $apartment->type()->associate($type);
        $apartment->save();
        $this->toast($request,$this->typeSuccess,$this->titleSuccess,'Квартира успешно изменено');
        return redirect()->route('apartment.Index');
    }

    public function uploadForm(Apartment $apartment)
    {
        return view('apartment.uploadImage', ['apartment' => $apartment]);
    }

    public function uploadImage(Request $request, Apartment $apartment)
    {
        $request->validate([
            'image' => 'required|file',
        ]);
        $photo = new Photo();
        $path = $this->pathImage($request);
        $photo->path = $path;
        $photo->alt = $request->input('alt');
        $photo->apartment()->associate($apartment);
        $photo->save();
        return redirect()->back();
    }

    public function destroyImage($id)
    {
        $photo = Photo::find($id);
        if (file_exists($photo->path)){
            unlink($photo->path);
        }
        $photo->delete();
        return redirect()->back();
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $request->validate([
            'apartment_id' => 'required|exists:apartments,id',
        ]);
        $apartment = Apartment::find($request->input('apartment_id'));
        $apartment->delete();
        $this->toast($request,$this->typeSuccess,$this->titleSuccess,'Категория успешно удалено');
        return redirect()->route('apartment.Index');
    }

    public function setActive()
    {
        $id = Input::get('id');
        $apartment = Apartment::find($id);
        if ($apartment->active){
            $apartment->active = false;
            $apartment->save();
            return redirect()->back();
        }else{
            $apartment->active = true;
            $apartment->save();
            return redirect()->back();
        }

    }

    public function rules()
    {
        return [
            'name' => 'required',
            'price' => 'required',
            'address' => 'required',
            'rooms' => 'required|int',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Имя обязательно',
            'price.required' => 'Цена обязательно',
            'address.required' => 'Адрес обязательно',
            'rooms.required' => 'Комнатами должны быть цифры',
        ];
    }

    public function pathImage(Request $request)
    {
        $image = $request->file('image');
        $size = getimagesize($image->path());
        if (!$size || count($size) < 2) {
            $this->toast($request,$this->typeError,$this->titleError,'Не удалось прочитать размер изображения');
            return redirect()->back();
        }
        $fileName = md5(time());
        $path = $image->move('uploads/images', $fileName.'.'.$image->getClientOriginalExtension());
        return $path;
    }

    public function toast(Request $request,$type,$title,$message)
    {
        $request->session()->flash('status',[
            'type' => $type,
            'title' => $title,
            'message' => $message,
        ]);
    }
}
