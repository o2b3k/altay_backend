<?php

namespace App\Http\Controllers;

use App\Models\Agent;
use Illuminate\Http\Request;
use Validator;

class AgentController extends Controller
{
    var $typeSuccess = 'success';
    var $typeError = 'error';
    var $titleSuccess = 'Успешно';
    var $titleError = 'Ошибка';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $agents = Agent::all();
        return view('agent.index', ['agents' => $agents]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('agent.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),$this->rules(),$this->messages());
        if ($validator->fails()){
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $agent = new Agent();
        if ($request->file('image') != null){
            $path = $this->pathImage($request);
            $agent->image = $path;
        }
        $agent->fio = $request->input('fio');
        $agent->phone = $request->input('phone');
        $agent->save();
        $this->toast($request,$this->typeSuccess,$this->titleSuccess,'Агент успешно сохранено');
        return redirect()->route('agent.Index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Agent  $agent
     * @return \Illuminate\Http\Response
     */
    public function show(Agent $agent)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Agent  $agent
     * @return \Illuminate\Http\Response
     */
    public function edit(Agent $agent)
    {
        return view('agent.edit',['agent' => $agent]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Agent  $agent
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Agent $agent)
    {
        $validator = Validator::make($request->all(),$this->rules(),$this->messages());
        if ($validator->fails()){
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        if ($request->file('image') != null){
            $path = $this->pathImage($request);
            $agent->image = $path;
        }
        $agent->fio = $request->input('fio');
        $agent->phone = $request->input('phone');
        $agent->save();
        $this->toast($request,$this->typeSuccess,$this->titleSuccess,'Ашент успешно изменено');
        return redirect()->route('agent.Index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Agent  $agent
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $request->validate([
            'agent_id' => 'required|exists:agents,id',
        ]);
        $agent = Agent::find($request->input('agent_id'));
        if ($agent->image != null){
            if (file_exists($agent->image)){
                unlink($agent->image);
            }
        }
        $agent->delete();
        $this->toast($request,$this->typeSuccess,$this->titleSuccess,'Агент успешно удалено');
        return redirect()->route('agent.Index');
    }

    public function rules()
    {
        return [
            'fio' => 'required|max:250',
            'phone' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'fio.required' => 'Имя обязательно',
            'phone.required' => 'Телефон обязательно',
        ];
    }

    public function pathImage(Request $request)
    {
        $image = $request->file('image');
        $size = getimagesize($image->path());
        if (!$size || count($size) < 2) {
            $this->toast($request,$this->typeError,$this->titleError,'Не удалось прочитать размер изображения');
            return redirect()->back();
        }
        $fileName = $image->getClientOriginalName();
        $path = $image->move('uploads/images', $fileName);
        return $path;
    }

    public function toast(Request $request,$type,$title,$message)
    {
        $request->session()->flash('status',[
            'type' => $type,
            'title' => $title,
            'message' => $message,
        ]);
    }
}
