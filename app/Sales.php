<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sales extends Model
{
    protected $table = 'sales';

    protected $fillable = ['data', 'rooms', 'series',
        'floor', 'area', 'district', 'address',
        'description', 'document', 'tel', 'price',
        's_price', 'agent'];
}
