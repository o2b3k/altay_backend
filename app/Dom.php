<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dom extends Model
{
    protected $table = 'doms';

    protected $fillable = ['data', 'type', 'district', 'address',
        'description', 'document', 'tel', 'price',
        's_price', 'agent'];
}
