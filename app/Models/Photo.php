<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    protected $table = 'photos';

    protected $fillable = ['path','alt'];

    public function apartment()
    {
        return $this->belongsTo('App\Models\Apartment');
    }
}
