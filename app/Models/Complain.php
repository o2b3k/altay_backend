<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Complain extends Model
{
    protected $table = 'complains';

    protected $fillable = ['name','phone','message'];
}
