<?php

namespace App\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

class Apartment extends Model
{
    use Sluggable;
    const exchange = 'exchange';
    const sale = 'sale';

    public static function getStatus($withName)
    {
        if ($withName){
            return [
                self::exchange => 'Обмен',
                self::sale => 'Продажа',
            ];
        }

        return [
            self::exchange,
            self::sale
        ];
    }

    protected $table = 'apartments';

    protected $fillable = ['number','name','rooms','series','floor',
                            'square','district','image','alt','active',
                            'status','address','price','desc','count',
                            'region_id','slug','category_id','agent_id'];

    public function category()
    {
        return $this->belongsTo('App\Models\Category');
    }

    public function region()
    {
        return $this->belongsTo('App\Models\Region');
    }

    public function agent()
    {
        return $this->belongsTo('App\Models\Agent');
    }

    public function photos()
    {
        return $this->hasMany('App\Models\Photo');
    }

    public function type()
    {
        return $this->belongsTo('App\Models\Type');
    }
    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => ['name','number']
            ]
        ];
    }

    public static function getExcerpt($str, $startPos = 0, $maxLength = 250) {
        if(strlen($str) > $maxLength) {
            $excerpt   = substr($str, $startPos, $maxLength - 6);
            $lastSpace = strrpos($excerpt, ' ');
            $excerpt   = substr($excerpt, 0, $lastSpace);
            $excerpt  .= ' ...';
        } else {
            $excerpt = $str;
        }

        return $excerpt;
    }

    public static function getShow($str, $startPos = 0, $maxLength = 250) {
        if(strlen($str) > $maxLength) {
            $excerpt   = substr($str, $startPos, $maxLength - 6);
            $lastSpace = strrpos($excerpt, ' ');
            $excerpt   = substr($excerpt, 0, $lastSpace);
            $excerpt  .= ' ...';
        } else {
            $excerpt = $str;
        }

        return $excerpt;
    }

    public static function getName($str, $startPos = 0, $maxLength = 50) {
        if(strlen($str) > $maxLength) {
            $excerpt   = substr($str, $startPos, $maxLength - 6);
            $lastSpace = strrpos($excerpt, ' ');
            $excerpt   = substr($excerpt, 0, $lastSpace);
            $excerpt  .= ' ...';
        } else {
            $excerpt = $str;
        }

        return $excerpt;
    }

}
